var Org = require('../models/organisation'),
    Mailer = require('../mailer'),
    endpoint = require('./endpoints');

// Creates a candidate in organisation candidates array + sends a confirmation mail
module.exports.invite = function(req, res) {
  var orgId = req.decoded.orgid,
      candidate = req.body.candidate,
      mailtext = req.body.mailtext;
  var candidatedata = {
    email: candidate.email,
    role: candidate.role
  };

  function onSave(err1, org) {
    if(err1)
      return endpoint.genError(res, err1);
    if(!org || !org.active) // No org found with that name or inactive org
      return endpoint.noOrg(res);

    var mailer = new Mailer(orgId, candidatedata.email),
        c = org.findCandidate(candidatedata.email);
    mailer.sendInvitation(c._id, mailtext,
      // error callback
      function (err2) {
        function onRemoveSuccess(org_item) {
          return endpoint.invitationError(res, err2);
        }
        function onRemoveError(err3) {
          return endpoint.genError(res, err3);
        }
        org.removeCandidate(c._id, onRemoveError, onRemoveSuccess);
      },
      // success callback
      function (resp) {
        return endpoint.success(res, resp, "Inbjudan skickades!");
      }
    ); // mailer

  }; // onSave

  function onFindById(err, org) {
    if(err)
      return endpoint.genError(res, err);

    // Check that candidate does not already exist
    var u = org.findUser(candidatedata.email),
        c = org.findCandidate(candidatedata.email);
    if(u || c)
      return endpoint.userCandidateAlreadyExist(res);

    // Ok to store among candidates
    org.candidates.push(candidatedata);
    org.save(onSave);
  }

  Org.model.findById(orgId, onFindById);
}; // create



module.exports.verify = function(req, res) {
  var orgid = req.body.orgid,
      candid = req.body.candid,
      hash = req.body.hash,
      userdata = req.body.userdata;

  function onFindById(err1, org) {
    if(err1)
      return endpoint.genError(res, err1);
    if(!org || !org.active) // No org found with that name or inactive org
      return endpoint.noOrg(res);
    if(!org.candidateConfirmMail(candid, hash)) // wrong hash
      return endpoint.hashError(res);

    // Everything correct and candidate exists
    org.activateCandidate(candid, userdata); // move candidate to users and activate
    org.save(function(err2, resp) {
      if(err2) // Save error
        return endpoint.genError(res, err2);
      return endpoint.success(res, resp, "Lyckad bekräftelse.");
    });
  };

  Org.model.findById(orgid, onFindById);

}; // confirm

