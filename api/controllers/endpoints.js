// This file contains all possible api endpoints (i.e json data that api sends
// on certain states, such as "data not found" and so on).
var jwt = require('jsonwebtoken'),
    config = require('../../config.app');

// Mail or Mongo error
exports.genError = function(res, err) {
  return res.json({
    success: false,
    error: err,
    message: "Okänt fel, var vänlig och kontakta support."
  });
};

// Error when storing to database
exports.validationError = function(res, err) {
  return res.json({
    success: false,
    error: err,
    message: "Något gick fel vid valideringen. Var vänlig att försök igen."
  });
};

// No org or org inactive
exports.noOrg = function(res) {
  return res.json({
    success: false,
    error: "No org found or inactive",
    message: "Det finns ingen sådan aktiv organisation registrerad.",
  });
};

exports.alreadyActivatedOrg = function(res) {
  return res.json({
    success: false,
    error: "Org already activated",
    message: "Okänt fel, var vänlig och kontakta support."
  });
};

// No user or user inactive
exports.noUser = function(res) {
  return res.json({
    success: false,
    error: "No user found or inactive",
    message: "Fel epost eller lösenord.",
  });
};

// No supplier found
exports.noSupplier = function(res) {
  return res.json({
    success: false,
    error: "No supplier found",
    message: "Ingen leverantör funnen.",
  });
};

// Error comparing passwords
exports.passwordError = function(res) {
  return res.json({
    success: false,
    error: "Could not compare passwords",
    message: "Fel epost eller lösenord."
  });
};

// Passwords did not match
exports.wrongPassword = function(res) {
  return res.json({
    success: false,
    error: "Wrong password",
    message: "Fel epost eller lösenord."
  });
};

// Success endpoint
exports.success = function(res, data, mess) {
  return res.json({
    success: true,
    data: data,
    message: mess
  });
};

// login user, success
exports.login = function(res, org, user) {
  var token = jwt.sign({
      orgid: org._id,
      name: user.name,
      userid: user._id,
      email: user.email,
      role: user.role,
      active: user.active
    }, config.super_secret, {
      expiresInMinutes: 1440 // 24 h TODO: specify (1440 minutes)
    });
  // Do not send users candidates and passwordhash to client
  return res.json({
    success: true,
    data: {org: org, user: user, token: token},
    message: "Lyckad inloggning."
  });
};

// Mail error
exports.mailError = function(res, err) {
  return res.json({
    success: false,
    error: err,
    message: "Något gick fel vid emailutskick. Försök igen och kontakta support om felet kvarstår."
  });
};

exports.invitationError = function(res, err) {
  return res.json({
    success: false,
    error: err,
    message: "Något gick fel vid utskick. Var god försök igen."
  });
};

exports.userCandidateAlreadyExist = function(res) {
  res.json({
    success: false,
    error: "Already confirmed user/candidate",
    message: "Användaren finns redan."
  });
};

exports.hashError = function(res) {
  return res.json({
    success: false,
    error: "Incorrect hash or id",
    message: "Okänt fel, var vänlig och kontakta support."
  });
};









