var Org = require('../models/organisation'),
    Mailer = require('../mailer'),
    helper = require('../helpers'),
    endpoint = require('./endpoints');

// Lists all organisations. Route: GET api/org
module.exports.list = function(req, res) {
  Org.model.find({}, function(err, orgs) {
    if(err)
      return endpoint.genError(res, err);
    return endpoint.success(res, orgs, "Alla organisationer.");
  });
};

// Get current org. Route: GET api/myorg
module.exports.myorg = function(req, res) {
  var orgid = req.decoded.orgid;

  function onFind(err, org) {
    if(err) // Unsuccessfull confirmation
      return endpoint.genError(res, err);
    if(!org || !org.active) // No org found with that id
      return endpoint.noOrg(res);
    return endpoint.success(res, org, "En org!");
  }
  Org.model.findById(orgid, onFind);
};

// Creates a organisation. Route: POST api/register
module.exports.register = function(req, res) {
  var data = req.body;
  var org = new Org.model({
    name: data.orgname,
    users: [{
      name: data.name,
      email: data.email,
      password: data.password,
      role: "creator"
    }]
  });

  function onOrgSave(err1, org) {
    if(err1)
      return endpoint.genError(res, err1);
    var mailer = new Mailer(org.id, data.email);
    function onError(err2) {
      org.remove(err3, function (doc) {
        if(err3) { // database remove error
          return endpoint.genError(res, err3);
        } else {
          return endpoint.mailError(res, err2); // email error
        }
      });
    }
    function onSuccess(info) {
      return endpoint.success(res, info, "Lyckad registrering.");
    }
    mailer.sendConfirmation(onError, onSuccess);
  }

  org.save(onOrgSave);
};



module.exports.confirm = function(req, res) {
  var orgid = req.params.id,
      hash = req.params.hash;

  function onFind(err1, org) {
    if(err1) // Unsuccessfull confirmation
      return endpoint.genError(res, err1);
    if(!org) // No org found with that id
      return endpoint.noOrg(res);
    if(org.active) // already activated org
      return endpoint.alreadyActivatedOrg(res);
    if(!org.creatorConfirmMail(hash)) // wrong hash
      return endpoint.hashError(res);

    // Everything correct
    org.active = true;
    org.activateAllUsers();
    org.save(function(err2, resp) {
      if(err2) // Save error
        return endpoint.genError(res, err2);
      return endpoint.success(res, resp, "Lyckad bekräftelse.");
    });
  };

  Org.model.findById(orgid, onFind);

};
