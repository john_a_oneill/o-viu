// Currently not used. Need to fix authentication and database schemas first.
var Org = require('../models/organisation'),
    Supplier = require('../models/supplier'),
    fs = require('fs'),
    endpoint = require('./endpoints');

// Lists all suppliers
module.exports.list = function(req, res, next) {
  var orgid = req.params.org_id;
  Org.model.findById(orgid).
  populate('suppliers').
  exec(function (err, org) {
    if(err || !org)
      return endpoint.genError(res, err);
    return endpoint.success(res, org.suppliers, "Alla leverantörer.");
  });
};

// One supplier
module.exports.get = function(req, res, next) {
  Supplier.model.findById(req.params.supplier_id, function (err, supplier) {
    if(err)
      return endpoint.genError(res, err);
    if(supplier)
      return endpoint.success(res, supplier, "En leverantör.");
    return endpoint.noSupplier(res);
  });
};

// get one supplier and update the info
module.exports.getOneAndUpdateSupplier = function(req, res, next) {
  // use our supplier model to find the supplier we want
  Supplier.model.findById(req.params.supplier_id, function (err, supplier) {
    console.log(req.params.supplier_id);
    if (err) {
      res.json({ message: req.body.name + ' kunde inte uppdaters!' });
      return next(err);
    }

    if (req.body.name)
      supplier.name = req.body.name; // update the supplier name
    if (req.body.orgnr)
      supplier.orgnr = req.body.orgnr; // update the supplier orgnr
    if (req.body.homepage_url)
      supplier.homepage_url = req.body.homepage_url; // update the supplier homepage url
    if (req.body.supplierManager)
      supplier.supplierManager = req.body.supplierManager; // update the supplier manager
    if (req.body.description)
      supplier.description = req.body.description; // update the supplier description
    if (req.body.rating)
      supplier.rating = req.body.rating; // update the supplier rating
    if (req.body.segment)
      supplier.segment = req.body.segment; // update the supplier segment
    if (req.body.category)
      supplier.category = req.body.category; // update the supplier category

    // save the supplier
    supplier.save(function (err) {
      if (err) {
        res.json({ 
          message: req.body.name + ' kunde inte uppdaters!',
          success: true 
        });
        return next(err);
      }
      res.json({ 
        message: req.body.name + ' uppdaterad!',
        success: true 
      });
    });

  });
};

// Create a new supplier
module.exports.create = function(req, res, next) {
  var supplierdata = {
    name: req.body.name,
    orgnr: req.body.orgnr,
    homepage_url: req.body.homepage_url,
    supplierManager: req.body.supplierManager,
    description: req.body.description,
    rating: req.body.rating,
    segment: req.body.segment,
    category: req.body.category
  };
  var orgid = req.params.org_id;
  Org.model.findById(orgid, function (err1, org) { // find org
    if (err1 || !org)
      return endpoint.genError(res, err1);
    Supplier.model.create(supplierdata, function (err2, supplier) { // create supplier
      if (err2)
        return endpoint.genError(res, err2);
      org.suppliers.push(supplier._id);
      org.save(function (err3) { // update org
        if (err3)
          return endpoint.genError(res, err3);
        return endpoint.success(res, supplier, "Leverantör skapades.");
      });
    });
  });
};

//Remove supplier by ID
module.exports.delete = function (req, res, next) {
  var supplierid = req.params.supplier_id;

  function onUpdate(err, nrOfAffected) {
    if(err)
      return enpoint.genError(res, err);
    var resp = "deleted " + nrOfAffected + " docs";
    return endpoint.success(res, resp, "Lyckad borttagning.");
  }
  function onRemove(err, supplier) {
    if (err)
      return enpoint.genError(res, err);
    var orgid = req.params.org_id;
    Org.model.update(
      { _id: orgid },
      { $pull: {suppliers: supplierid} },
      onUpdate
    );
  }
  Supplier.model.remove({ _id: supplierid }, onRemove);
};
