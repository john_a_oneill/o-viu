var mongoose = require('mongoose'),
    endpoint = require('./endpoints'),
    Grid = require('gridfs-stream'),
    gfs,
    gridSchema;

//Create new Grid Schema
gridSchema = new mongoose.Schema({}, {
  strict: false
});
//Assign Grid schema to gfs variable
gfs = Grid(mongoose.connection.db, mongoose.mongo);
Grid = mongoose.model('Grid', gridSchema, 'uploads.files');

module.exports.list = function (req, res, next) {
  return Grid.find({}, function (err, files) {
    if (err) {
      return endpoint.genError(res, err);
    }
    if (files.length > 0) {
      return res.send(files);
    } else {
      return res.send('No files found');
    }
  });
};

module.exports.get = function(req, res, next) {
  return Grid.findOne({
    _id: req.params.file_id
  }, function (err, file) {
    var readStream;
    if (err) {
      return endpoint.genError(res, err);
    }
    if (file) {
      readStream = gfs.createReadStream({
        _id: file._id,
        root: 'uploads'
      });
      return readStream.pipe(res);
    } else {
      return res.send('File not found');
    }
  });
};

module.exports.create = function (req, res, next) {
  req.busboy.on('file', function (field, file, filename, encoding, mimetype) {
    var writeStream;
    file.on('data', function (data) {
      return //console.log('data: ', data.length);
    });
    file.on('end', function () {
      return //console.log('end: ', field);
    });
    writeStream = gfs.createWriteStream({
      _id: mongoose.Types.ObjectId(),
      filename: filename,
      mode: 'w',
      content_type: mimetype,
      root: 'uploads'
    });
    return file.pipe(writeStream);
  });
  req.busboy.on('finish', function () {
    return res.end();
  });
  return req.pipe(req.busboy);
};
