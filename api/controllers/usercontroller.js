var Org = require('../models/organisation'),
    User = require('../models/user'),
    jwt = require('jsonwebtoken'),
    Mailer = require('../mailer'),
    endpoint = require('./endpoints'),
    helper = require('../helpers'),
    config = require('../../config.app');

// Lists all users in a org. Route: GET api/user/:org_name
module.exports.list = function(req, res) {
  var orgName = req.params.org_name;
  // Org.find returns a array of orgs even if one is found
  Org.model.findOne({ name: orgName }, function (err, org) {
    if(err) {
      return next(err);
    } else {
      if(org) {
        console.log(org.users);
        return res.send({
          success: true,
          data: org.users
        });
      } else {
        return res.json({
          success: false,
          error: "org",
          message: "Det finns ingen organisation med detta namn"
        });
      }
    }
  });
};

// One user
module.exports.get = function(req, res, next) {
  var orgId = req.params.org_id;
  var user_id = req.params.user_id;
  Org.model.findById(orgId, function (err, org) {
    if(err)
      return endpoint.genError(res, err);
    if(org)
      var data = org.users.id(user_id);
      return endpoint.success(res, data, "En användare.");
    return endpoint.noSupplier(res);
  });
};

// get one supplier and update the info
module.exports.getOneAndUpdateUser = function(req, res, next) {
  // use our supplier model to find the supplier we want
  Org.model.findById(req.params.supplier_id, function (err, supplier) {
    if (err) {
      return next(err);
    }

    if (req.body.name)
      supplier.name = req.body.name; // update the supplier name
    if (req.body.orgnr)
      supplier.orgnr = req.body.orgnr; // update the supplier orgnr
    if (req.body.homepage_url)
      supplier.homepage_url = req.body.homepage_url; // update the supplier homepage url
    if (req.body.supplierManager)
      supplier.supplierManager = req.body.supplierManager; // update the supplier manager
    if (req.body.description)
      supplier.description = req.body.description; // update the supplier description
    if (req.body.rating)
      supplier.rating = req.body.rating; // update the supplier rating
    if (req.body.segment)
      supplier.segment = req.body.segment; // update the supplier segment
    if (req.body.category)
      supplier.category = req.body.category; // update the supplier category

    // save the supplier
    supplier.save(function (err) {
      if (err) {
        return next(err);
      }
      res.json({ message: 'Supplier uppdaterad!' });
    });

  });
};


// Get current user. Route: GET api/me
module.exports.me = function(req, res) {
  return res.json(req.decoded);
};

//Remove User by ID
module.exports.delete = function (req, res, next) {
  var user = {};
  var fromUserID = req.decoded.userid; //Gets the userID of the user sending the delete request
  var user_id = req.params.user_id;
  var orgid = req.params.org_id;

  function onUpdate(err, nrOfAffected) {
    if(err) {
      return endpoint.genError(res, 'Error onUpdate in module.exports.delete');
    }
    var resp = "deleted " + nrOfAffected + " docs";
    return endpoint.success(res, resp, "Lyckad borttagning.");
  }
  //Check if the user is not trying to delete him/her self then update Org schema
  if (fromUserID != user_id) {
    Org.model.update(
      { _id: orgid },
      { $pull: 
        { 
          users: { _id: user_id}
        } 
      },
      onUpdate
    );
  } else {
    return endpoint.genError(res, 'Användare försöker ta bort sig själv');
  }

};

// Authenticates a user. Route: POST api/login
module.exports.login = function(req, res) {
  var orgname = req.body.orgname,
      email = req.body.email,
      password = req.body.password;

  function onFindOne(err, org) {
    if(err) { 
      return endpoint.genError(res, err);
    }
    if(!org || !org.active) {
      return endpoint.noOrg(res); // No org found with that name or inactive org
    } 
    var user = org.findUser(email)
    if(!user || !user.active) {
      return endpoint.noUser(res); // No user found (Security: do not display that fact)
    } 
    function onComparePassword(err, isMatch) {
      if(err) {
        return endpoint.passwordError(res);
      }
      if(!isMatch) {
        return endpoint.wrongPassword(res);  // Not valid password (Security: do not display that fact)
      }
      console.log(user);
      return endpoint.login(res, org, user); // Everything OK, fire the login endpoint
    } // onComparePassword
    user.comparePasswords(password, onComparePassword);
  } // onFindOne
  
  Org.model.findOne({ name: orgname }, onFindOne);

}; // login



module.exports.recoverpassword = function(req, res) {
  var orgname = req.body.orgname,
      email = req.body.email;

  function onFindOne(err, org) {
    if(err)
      return endpoint.genError(res, err);
    if(!org || !org.active) // No org found with that name or inactive org
      return endpoint.noOrg(res);
    var user = org.findUser(email);
    if(!user || !user.active) // No user found (Security: do not display that fact)
      return endpoint.noUser(res);

    var mailer = new Mailer(org._id, email);
    function onError(err1) { // email error
      return endpoint.mailError(res, err1);
    }
    function onSuccess(info) {
      return endpoint.success(res, info, "Var god och följ intruktionerna vi skickade till dig.");
    }
    mailer.sendPasswordRecovery(user._id, onError, onSuccess);

  } // onFindOne

  Org.model.findOne({ name: orgname }, onFindOne);

}; // recoverpassword



module.exports.resetandlogin = function(req, res) {
  var password = req.body.password,
      orgid = req.body.orgid,
      userid = req.body.userid,
      hash = req.body.hash;

  function onFindById(err, org) {
    if(err)
      return endpoint.genError(res, err);
    if(!org || !org.active) // No org found with that name or inactive org
      return endpoint.noOrg(res);
    var user = org.users.id(userid);
    if(!user || !user.active) // No user found (Security: do not display that fact)
      return endpoint.noUser(res);

    if(helper.compare(user.email, hash)) { // success store new password
      user.password = password;
      org.save(function(error, org) {
        if(error)
          return endpoint.genError(res, error);
        return endpoint.login(res, org, user);
      });
    } else {
      return endpoint.genError(res, "Wrong hash on email comp.");
    }

  } // onFindOne

  Org.model.findById(orgid, onFindById);

}; // resetandlogin
