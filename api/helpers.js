var crypto = require('crypto');

// Update created time only during creation
exports.updateTime = function(next) {
  var now = new Date(),
      model = this;
  model.updated_at = now;
  if(!model.created_at)
    model.created_at = now;
  next();
};

exports.passwordValidate = function(value) {
  return /.{8,}/.test(value);
};

exports.emailValidate = function(value) {
  var emailRegex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return emailRegex.test(value);
};

exports.roleValidate = function(value) {
  var roleRegex = /\b(creator|admin|editor|viewer)\b/;
  return roleRegex.test(value);
};

// org_id, creator mail
exports.confirmlink = function(mail) {
  return crypto.createHash('sha1').update(mail).digest('hex');
};

// creator mail, hash
exports.compare = function(mail, hash) {
  var orig = crypto.createHash('sha1').update(mail).digest('hex');
  return orig === hash;
};

