var nodemailer = require('nodemailer'),
    transporter = nodemailer.createTransport(),
    helper = require('./helpers');

function Mailer(id, mail) {
  this.id = id;
  this.mail = mail;
}

Mailer.prototype.sendConfirmation = function(onError, onSuccess) {
  var hash = helper.confirmlink(this.mail),
      confirmlink = "http://app.o-viu.com/confirm/" + this.id + "/" + hash,

      mailOptions = {
        from: 'no-reply@o-viu.com',
        to: this.mail,
        subject: 'Välkommen till O-viu',
        text: "Bekräfta din användare genom att klicka på länken: " + confirmlink, // plaintext body
        html: "Bekräfta din användare genom att klicka på länken: " + confirmlink + " ✔" // html
      };

  transporter.sendMail(mailOptions, function(err, info) {
    if(err) {
      console.log("Mail error: " + err);
      onError(err);
    } else {
      onSuccess(info);
    }
  });
};

Mailer.prototype.sendInvitation = function(candId, mailtext, onError, onSuccess) {
  var hash = helper.confirmlink(this.mail),
      confirmlink = "http://app.o-viu.com/invitationconfirmation/" + this.id + "/" + candId + "/" + hash,

      mailOptions = {
        from: 'no-reply@o-viu.com',
        to: this.mail,
        subject: 'Välkommen till O-viu',
        text: mailtext + "\n\nFölj länken: " + confirmlink + " om du vill gå med i O-viu.", // plaintext body
        html: mailtext + "<br><br>Följ länken: " + confirmlink + " ✔ om du vill gå med i O-viu." // html
      };

  transporter.sendMail(mailOptions, function(err, info) {
    if(err) {
      console.log("Mail error: " + err);
      onError(err);
    } else {
      onSuccess(info);
    }
  });
};

Mailer.prototype.sendPasswordRecovery = function(userid, onError, onSuccess) {
  var hash = helper.confirmlink(this.mail),
      confirmlink = "http://app.o-viu.com/reset-password/" + this.id + "/" + userid + "/" + hash,
      mailtext = "Har du glömt ditt lösenord? Inga problem. Det händer alla någon gång.\
 Vi har gjort det enkelt för dig att återställa ditt lösenord.",

      mailOptions = {
        from: 'no-reply@o-viu.com',
        to: this.mail,
        subject: 'Återställ ditt lösenord på O-viu',
        text: mailtext + "\n\nÅterställ ditt lösenord genom att följa lönken: " + confirmlink, // plaintext body
        html: mailtext + "<br><br>Återställ ditt lösenord genom att följa lönken: " + confirmlink + "✔ " // html
      };

  transporter.sendMail(mailOptions, function(err, info) {
    if(err) {
      console.log("Mail error: " + err);
      onError(err);
    } else {
      onSuccess(info);
    }
  });

};

module.exports = Mailer;
