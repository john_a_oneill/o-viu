// Invited candidates by admins
var mongoose = require('mongoose'),
    helper = require('../helpers');

// TODO: make script that cleans out any candidate object older than 2 weeks
var CandidateSchema = mongoose.Schema({
  email:      { type: String, required: true },
  role:       { type: String, required: true }, // Creator, Admin, Editor and Viewer
  created_at: { type: Date, default: Date.now }
});

// Check that role is either "creator", "admin", "editor", or "viewer"
CandidateSchema.path('role').validate(helper.roleValidate, 'RoleErr');

module.exports.schema = CandidateSchema;
module.exports.model = mongoose.model('Candidate', CandidateSchema);
