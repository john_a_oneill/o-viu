var mongoose = require('mongoose'),
    bcrypt = require('bcrypt-nodejs'),
    helper = require('../helpers'),
    User = require('./user'),
    Candidate = require('./candidate'),
    Supplier = require('./supplier');

// Organisation Schema

var OrgSchema = mongoose.Schema({
  name:       { type: String, required: true, unique: true},
  active:     { type: Boolean, default: false },
  users:      [ User.schema ],
  candidates: [ Candidate.schema ],
  // Store only refs to Supplier to store space in document
  suppliers:  [{ type: mongoose.Schema.Types.ObjectId, ref: 'Supplier'}],
  created_at: { type: Date },
  updated_at: { type: Date }
});

OrgSchema.pre('save', helper.updateTime);

OrgSchema.pre('update', function() {
  this.updated_at = Date.now();
});

OrgSchema.methods.findUser = function(mail) {
  var org = this;
  var user = org.users.filter(function (u) {
    return u.email === mail;
  }).pop();
  return user;
};

OrgSchema.methods.findCandidate = function (mail) {
  var org = this;
  var cand = org.candidates.filter(function (u) {
    return u.email === mail;
  }).pop();
  return cand;
};

// moves candidate to users and removes candidate
OrgSchema.methods.activateCandidate = function (id, userdata) {
  var org = this,
      cand = org.candidates.id(id);
  org.users.push({
    name: userdata.name,
    email: cand.email,
    active: true,
    role: cand.role,
    password: userdata.password
  });
  org.candidates.pull(id);
};

// TODO: Test this
OrgSchema.methods.removeCandidate = function (id, onRemoveError, onRemoveSuccess) {
  var org = this;
  org.candidates.pull(id);
  org.save(function (err) {
    if(err) {
      onRemoveError(err);
    } else {
      onRemoveSuccess(org);
    }
  });
};

OrgSchema.methods.creatorConfirmMail = function (hash) {
  var org = this;
  var user = org.users.filter(function (user) {
    return user.role === "creator";
  }).pop();
  if(user) {
    return helper.compare(user.email, hash);
  } else {
    return false;
  }
};

OrgSchema.methods.candidateConfirmMail = function(id, hash) {
  var cand = this.candidates.id(id);
  if(cand) {
    return helper.compare(cand.email, hash);
  } else {
    return false;
  }
};

OrgSchema.methods.activateAllUsers = function() {
  var org = this;
  org.users.forEach(function (user) {
    user.active = true;
  });
};

module.exports.schema = OrgSchema;
module.exports.model = mongoose.model('Org', OrgSchema);
