// Suppliers
var mongoose = require('mongoose'),
    helper = require('../helpers');

//The contact person schema will be a part of the SupplierSchema below.
var ContactPersonSchema = mongoose.Schema({
  name:       { type: String, required: true },
  email:      { type: String },
  telnr:      { type: Number, max: 20 },
  created_at: { type: Date },
  updated_at: { type: Date }
});
ContactPersonSchema.pre('save', helper.updateTime);

ContactPersonSchema.pre('update', function() {
  this.updated_at = Date.now();
});

//The supplier address schema will be a part of the SupplierSchema below.
var SupplierAddressSchema = mongoose.Schema({
  street:     { type: String, required: true },
  zipcode:    { type: String },
  city:       { type: String, required: true },
  country:    { type: String },
  created_at: { type: Date },
  updated_at: { type: Date }
});
SupplierAddressSchema.pre('save', helper.updateTime);

SupplierAddressSchema.pre('update', function() {
  this.updated_at = Date.now();
});

//This schema saves all suppliers
var SupplierSchema = mongoose.Schema({
  name:             { type: String, required: true },
  orgnr:            { type: String, required: true, unique: true },
  homepage_url:     { type: String },
  supplierManager:  { type: String },
  logoFileID:       { type: String }, 
  contacts:         [ ContactPersonSchema ],
  // TODO: Should segment and category exist and then how?
  //segment:          { type: mongoose.Schema.Types.ObjectId, ref: 'segment.bottomsegmentschema'},
  segment:          { type: String },
  category:         { type: String },
  //category:         { type: mongoose.Schema.Types.ObjectId, 'categorySchema' },
  description:      { type: String },
  address:          [ SupplierAddressSchema ], // TODO: are these many (array) or one?
  rating:           { type: Number, min: 1, max: 5 },
  created_at:       { type: Date },
  updated_at:       { type: Date }
});
SupplierSchema.pre('save', helper.updateTime);

SupplierSchema.pre('update', function() {
  this.updated_at = Date.now();
});


module.exports.schema = SupplierSchema;
module.exports.model = mongoose.model('Supplier', SupplierSchema);
