// Creators, Admins, Editors, Viewers
var mongoose = require('mongoose'),
    helper = require('../helpers'),
    bcrypt = require('bcrypt-nodejs'),
    SALT_WORK_FACTOR = 10;

var UserSchema = mongoose.Schema({
  name:               { type: String, required: true },
  email:              { type: String, required: true },
  active:             { type: Boolean, default: false},
  role:               { type: String, required: true }, // Creator, Admin, Editor and Viewer
  password:           { type: String, required: true },
  lastLoginAttempt:   { type: Date },
  created_at:         { type: Date },
  updated_at:         { type: Date }
});

// Hashes password before saving so the password is not out in the open.
UserSchema.pre('save', function(next) {
  var user = this;
  if(!user.isModified('password')) { // only hash if modified
    helper.updateTime.call(user, next);
  } else {
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err1, salt) {
      if (err1)
        return next(err1);
      bcrypt.hash(user.password, salt, null, function(err2, hash) {
        if(err2)
          return next(err2);
        user.password = hash; // changes the password -> hash
        return helper.updateTime.call(user, next);
      });
    });
  }
});

//Mongoose middleware for updating the lastLoginAttempt to current date and time when comparing passwords.
UserSchema.pre('comparePasswords', function (next) {
  this.lastLoginAttempt = Date.now();
  next();
});

//Middleware for updating updated_at to current time and date.
UserSchema.pre('update', function() {
  this.updated_at = Date.now();
});

// Helps to compare users passwords when logging in (Authentication)
UserSchema.methods.comparePasswords = function(candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
    if (err) return cb(err);
    cb(null, isMatch);
  });
};

// Check that password has a min-length of 8 chars
UserSchema.path('password').validate(helper.passwordValidate, 'PasswordErr');

// Check that email is valid regular expression
UserSchema.path('email').validate(helper.emailValidate, 'EmailErr');

// Check that role is either "creator", "admin", "editor", or "viewer"
UserSchema.path('role').validate(helper.roleValidate, 'RoleErr');

module.exports.schema = UserSchema;
module.exports.model = mongoose.model('User', UserSchema);
