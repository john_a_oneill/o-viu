var jwt = require('jsonwebtoken'),
    config = require('../config.app');

// Controllers for specific resources
var CandidateController = require('./controllers/candidatecontroller'),
    UserController = require('./controllers/usercontroller'),
    UploadController = require('./controllers/uploadcontroller'),
    SupplierController = require('./controllers/suppliercontroller'),
    OrgController = require('./controllers/organisationcontroller');



// We define all routes here on the router given by server.js
// This is for separation of concerns only.
module.exports = function (router) {

  // PUBLIC ROUTES

  router.route('/login')
    .post(UserController.login);

  // register organisation and creator
  router.route('/register')
    .post(OrgController.register);

  // confirm link sent to creator, now activate org and creator
  router.route('/confirm/:id/:hash')
    .get(OrgController.confirm);

  // confirm link sent to a user, now activate user
  router.route('/confirm')
    .post(CandidateController.verify);

  // user forgot password, now send recovery instruction through link
  router.route('/recoverpassword')
    .post(UserController.recoverpassword);

  // user entered new password and authenticated through params
  router.route('/resetandlogin')
    .post(UserController.resetandlogin);

  router.route('/testing')
    .get(OrgController.list);

  // router.route('/upload')
  //   //Return all metadata for images as JSON
  //   .get(UploadController.list)
  //   // upload file to add to supplier, access roles: admin and editor
  //   .post(verifyAccess(["creator", "admin", "editor"], UploadController.create));

  // router.route('/upload/:file_id')
  //   //Get one image
  //   .get(UploadController.get);

  // PRIVATE ROUTES

  // Middleware before private routes
  // Always verify the token before accessing the api routes
  router.use(function (req, res, next) {
    var token = req.headers['x-access-token'] || req.params.token || req.body.token;
    if(token) {
      // asynchronous token verification
      jwt.verify(token, config.super_secret, function(err, decoded) {
        if(err) { // NO: False token
          return res.status(403).send({
            success: false,
            error: "Unauthorized access: false token",
            message: "Obehöriga äger ej tillträde."
          });
        } else { // Yes: Ok token (pass token on, on req param)
          req.decoded = decoded;
          next();
        }
      });
    } else { // NO: No token
      return res.status(403).send({
        success: false,
        error: "Unauthorized access: no token",
        message: "Obehöriga äger ej tillträde."
      });
    }
  });

  // Check that user requesting or posting data is authorized
  // passes req and res to controllerFunc if user belongs to accessgroups
  function verifyAccess(accessgroups, controllerFunc) {
    return function(req, res) {
      var role = req.decoded.role;
      // check that user has access role to make api call
      if(accessgroups.indexOf(role) > -1) {
        controllerFunc(req, res);
      } else {
        return res.json({
          success: false,
          error: "Unauthorized access: inusufficient privileges",
          message: "Du har inte behörighet för detta."
        });
      }
    };
  }

  router.route('/me')
    // sends token decoded, access roles: all
    .get(UserController.me);

  //Gets the current organisation
  router.route('/myorg')
    // sends org decoded, access roles: all
    .get(OrgController.myorg);

  //Route for posting a new user
  router.route('/user')
    // admin sends invitation, access roles: creator, admin
    .post(verifyAccess(["creator", "admin"], CandidateController.invite));

  //Route for listing all users in an orginsation by sending org name as parameter
  //TODO: Post data variable, UserController.create does not exist
  router.route('/user/:org_name')
    // returns all users, access roles: all
    .get(UserController.list);

  //Route for listing one users in an orginsation by sending org name as parameter
  router.route('/user/:org_id/:user_id')
    // returns one users, access roles: all
    .get(UserController.get)
    // admin sends invitation, access roles: creator, admin
    .put(verifyAccess(["creator", "admin"], UserController.getOneAndUpdateUser))
    // Delete one user by ID, access roles: creator, admin
    .delete(verifyAccess(["creator", "admin"], UserController.delete));

  router.route('/supplier/:org_id')
    // GET (Read) all suppliers for user
    .get(SupplierController.list)
    // Create a new supplier, access roles: creator, admin and editor
    .post(verifyAccess(["creator", "admin", "editor"], SupplierController.create));

  // Route for one supplier
  router.route('/supplier/:org_id/:supplier_id')
    // GET (Read) one suppliers for user
    .get(SupplierController.get)
    // TODO: is this one used?
    // update one supplier on ID, access roles: creator, admin and editor
    .put(verifyAccess(["creator", "admin", "editor"], SupplierController.getOneAndUpdateSupplier))
    // Delete one supplier by ID, access roles: creator, admin and editor
    .delete(verifyAccess(["creator", "admin", "editor"], SupplierController.delete));

  router.route('/upload')
    //Return all metadata for images as JSON
    .get(UploadController.list)
    // upload file to add to supplier, access roles: admin and editor
    .post(verifyAccess(["creator", "admin", "editor"], UploadController.create));

  router.route('/upload/:filename')
    //Get one image
    .get(UploadController.get);

  return router;
};
