(function(){
  "use strict";

/**
 * This file holds all front-end routing logic and is the entry point
 */
angular.module('supplierApp', [
  // External resources
  'ngResource',
  'ui.router',
  'ngAnimate',
  'ui.bootstrap',
  'snap',
  'angularFileUpload',
  // Internal resources
  'templates-main',
  'common',
  'modules.home',
  'modules.login',
  'modules.navbar',
  'modules.orgconfirmation',
  'modules.suppliers',
  'modules.resetpassword',
  'modules.useradministration'
])

// TODO: Use Modernizr to see if browser has: Localstorage, and other stuff that
// application requires, if not display message to upgrade browser to
// appropriate version.

// This controller mainly controlls authentication of the app and what resources
// the user is allowed to see if logged in or not.
.controller('mainCtrl', ['$rootScope', '$log', '$state', 'userService',
  function($rootScope, $log, $state, userService) {
    //Configure the snap menu
    $rootScope.snapOpts = {
      disable: 'right',
      maxPosition: 200
    };

    //For all state changes check if user is logged in
    $rootScope.$on('$stateChangeStart',
      function(event, toState, toParams, fromState, fromParams) {
        //Clear state and title for breadcrumb in header
        $rootScope.firstStateTitle = null;
        $rootScope.firstState = null;
        $rootScope.secondStateTitle = null;
        $rootScope.secondState = null;
        $rootScope.clickHandler = null;

        //Store status true or false of user is logged in in rootScope
        var loggedIn = userService.isLoggedIn();
        $rootScope.isLoggedIn = loggedIn;

        //Get the current user role and store it in rootScope
        if(loggedIn) {
          userService.getCurrentUser(
            function (err) {
            }, function (resp) {
              var currentUser = resp.data;
              if(!currentUser) { // no data retreived
              } else {
              $rootScope.userRole = currentUser.role; //Store user role in rootScope
              $rootScope.userName = currentUser.name; //Store user name in rootScope
              $rootScope.userId = currentUser.userid; //Store user ID in rootScope
              }
            }
          );
        }
        
        if ((toState.access.type === "private") && !loggedIn) {
          event.preventDefault(); //prevents from resolving requested url
          $state.go('login');
        }
      }
    );

    // logout userfunction on every child scope
    $rootScope.logoutUser = function() {
      userService.logout();
    };
  }
])

// Configuration of application
.config(['$locationProvider', '$httpProvider', '$urlRouterProvider',
  function($locationProvider, $httpProvider, $urlRouterProvider) {
    // enable html5Mode for pushstate ('#'-less URLs)
    $locationProvider.html5Mode(true);

    // attach our auth interceptor to all the http requests
    $httpProvider.interceptors.push('authInterceptorService');

    // redirect users to 404 if not found route
    $urlRouterProvider.when('/404',
      ['$state', 'userService',
        function ($state, userService) {
          if(userService.isLoggedIn) $state.go('hem');
          else $state.go('login');
        }
      ]
    );
    $urlRouterProvider.otherwise('/404');
  }
]);

angular.module('common', [
  // Services
  'common.services.userservice',
  'common.services.orgservice',
  'common.services.supplierservice',
  'common.services.authservice',
  'common.services.authinterceptorservice',
  // Directives
  'common.directives.passwordverify',
  // Filter
  'common.filters.descriptionfilter',
  'common.filters.segmentFilter',
  'common.filters.capitalizeFirst'
]);

angular.module('common.directives.passwordverify', [])

.directive('passwordVerify', function() {
  return {
    require: 'ngModel',
    restrict: 'A',
    scope: {
      passwordVerify: '='
    },
    link: function(scope, elem, attrs, ctrl) {
      scope.$watch(
        // value function returns the value being watched
        function() {
          var combined;
          if (scope.passwordVerify || ctrl.$viewValue) {
            combined = scope.passwordVerify + '_' + ctrl.$viewValue;
          }
          return combined;
        },
        // The listener function: Called only when the value from the current
        // combined and the previous call to combined are not
        // equal.
        function(value) {
          if (value) {
            ctrl.$parsers.unshift(function(viewValue) {
              var origin = scope.passwordVerify;
              if (origin !== viewValue) {
                ctrl.$setValidity("passwordVerify", false);
                return undefined;
              } else {
                ctrl.$setValidity("passwordVerify", true);
                return viewValue;
              }
            });
          }
        }
      );
    }

  };
});

angular.module('common.filters.capitalizeFirst', [])

 .filter('capitalizeFirst', [function () {
    return function (str) {
        str = str || '';
        return str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase();
    };
}]);
angular.module('common.filters.descriptionfilter', [])

 .filter('descriptionFilter', ['$filter', function($filter) {
   return function(input, limit) {
      if (! input) return;
      if (input.length <= limit) {
          return input;
      }

      return $filter('limitTo')(input, limit) + '...';
   };
}]);
angular.module('common.filters.segmentFilter', [])

 .filter('segmentFilter', [function () {
    return function( items, types) {
	    var filtered = [];
	    
	    angular.forEach(items, function(item) {
	       if(types.luxury == false && types.double_suite == false) {
	          filtered.push(item);
	        }
	        else if(types.luxury == true && types.double_suite == false && item.type == 'luxury'){
	          filtered.push(item);
	        }
	        else if(types.double_suite == true && types.luxury == false && item.type == 'double suite'){
	          filtered.push(item);
	        }
	    });
	  
	    return filtered;
	};
}]);
// This service is for intercepting all http requests and authenticate the user
angular.module('common.services.authinterceptorservice', [])

.factory('authInterceptorService', ['$q', 'authService',
    function($q, authService) {
      var authInterceptorService = {};

      // Lets us intercept requests before they are sent
      // Gets the token and attaches to the header for the
      // server to verify the user
      authInterceptorService.request = function(config) {
        var token = authService.getToken();
        if(token)
          config.headers['x-access-token'] = token;
        return config;
      };

      // Lets us handle unauthorized requests (responseerrors)
      authInterceptorService.responseError = function(res) {
        if(res.status == 403)
          authService.clearToken();
        // return the errors from the server as a promise
        return $q.reject(res);
      };

      return authInterceptorService;
    }
]);

// This service is for handling user requests (returns promises)
angular.module('common.services.authservice', [])

.factory('authService', ['$window', function($window) {
  var authService = {};

  authService.getToken = function() {
    return $window.localStorage.getItem('o-viu-signature');
  };

  authService.setToken = function(token) {
    $window.localStorage['o-viu-signature'] = token;
  };

  authService.clearToken = function() {
    $window.localStorage.removeItem('o-viu-signature');
  };

  return authService;

}]);

// This service is for handling user requests (returns promises)
angular.module('common.services.orgservice', [])

.factory('orgService', ['$http', '$log', '$q', '$timeout', '$window', '$rootScope',
  function($http, $log, $q, $timeout, $window, $rootScope) {
    var orgService = {};

    orgService.create = function (userdata, errorhandler, callback) {
      $http.post('/api/register', userdata)
      .then(callback)
      .catch(errorhandler);
    };

    orgService.confirm = function(id, hash, errorhandler, callback) {
      $http.get('/api/confirm/' + id + '/' + hash)
      .then(callback)
      .catch(errorhandler);
    };

    orgService.getCurrentOrg = function(errorhandler, callback) {
      $http.get('/api/myorg')
      .then(callback)
      .catch(errorhandler);
    };

    return orgService;
  }
]);

angular.module('common.services.supplierservice', [])

// TODO: How are these factories later used? Should there be a array in
// suppliers schema or on organisation?
.factory('segmentFactory', [function () {
  var segments = 
  [{
    id: 1,
    name: 'partner'
  }, {
    id: 2,
    name: 'föredragna'
  }, {
    id: 3,
    name: 'övriga'
  }];
  return segments;
}])

.factory('categoryFactory', [function () {
  var categories = 
  [{
    id: 1,
    name: 'produktion'
  }, {
    id: 2,
    name: 'råmaterial'
  }, {
    id: 3,
    name: 'logistik'
  }, {
    id: 4,
    name: 'entreprenad'
  }, {
    id: 5,
    name: 'fastigheter'
  }, {
    id: 6,
    name: 'kontorsmaterial'
  }, {
    id: 7,
    name: 'IT'
  }];
  return categories;
}])

// TODO: Hook up this to supplier handling
.factory('supplierFactory',

//  ['$http', '$rootScope', '$resource',
//    function ($http, $rootScope, $resource) {

  ['$http', '$q', '$log', '$resource', '$location', 'authService', 'orgService',
    function ($http, $q, $log, $resource, $location, authService, orgService) {
      var supplierService = {};

      supplierService.suppliers = [];

      // Get all suppliers
      supplierService.get = function () {
        // var orgid = $rootScope.org._id;
        // return $http.get('/api/supplier/' + orgid);
        orgService.getCurrentOrg(
          function (err) { /* TODO: do something and do same thing below */ },
          function (resp1) {
            if (resp1.data.success) {
              $http.get('/api/supplier/' + resp1.data.data._id)
              .then(function (resp2) {
                if (resp2.data.success)
                  supplierService.suppliers = resp2.data.data;
              });
              // TODO: do it here
            }
          }
        );
      };

      //Create new supplier
      supplierService.create = function (supplierdata, errorhandler, callback) {
        orgService.getCurrentOrg(
        errorhandler,
        function (resp) {
          $http.post('/api/supplier/' + resp.data.data._id, supplierdata)
          .then(callback)
          .catch(errorhandler)
        });
      };

      //Get one supplier from params
      supplierService.getOne = function (supplier_id, errorhandler, callback) {
        orgService.getCurrentOrg(
        errorhandler,
        function (resp) {
          $http.get('/api/supplier/' + resp.data.data._id + '/' + supplier_id)
          .then(callback)
          .catch(errorhandler);
        });
      };

      //Get one supplier from params
      supplierService.update = function (supplier_id, supplierdata, errorhandler, callback) {
        orgService.getCurrentOrg(
        errorhandler,
        function (resp) {
          $http.put('/api/supplier/' + resp.data.data._id + '/' + supplier_id, supplierdata)
          .then(callback)
          .catch(errorhandler);
        });
      };

      //Delete supplier
      supplierService.delete = function (supplier_id, errorhandler, callback) {
        orgService.getCurrentOrg(
        errorhandler,
        function (resp) {
          $http.delete('/api/supplier/' + resp.data.data._id + '/' + supplier_id)
          .then(callback)
          .catch(errorhandler);
        });
      };

      return supplierService;
    }
  ]
);

// This service is for handling user requests (returns promises)
angular.module('common.services.userservice', [])

.factory('userService',
  ['$http', '$q', '$log', '$rootScope', '$state', '$window', 'authService', 'orgService',
    function ($http, $q, $log, $rootScope, $state, $window, authService, orgService) {
      var userService = {};

      // lists all users
      userService.all = function (orgName, errorhandler, callback) {
        $http.get('/api/user/' + orgName)
        .then(callback)
        .catch(errorhandler);
      };

      //Get one user from params
      userService.getOne = function (user_id, errorhandler, callback) {
        orgService.getCurrentOrg(
        errorhandler,
        function (resp) {
          $http.get('/api/user/' + resp.data.data._id + '/' + user_id)
          .then(callback)
          .catch(errorhandler);
        });
      };

      //Delete one user from params
      userService.delete = function (user_id, errorhandler, callback) {
        orgService.getCurrentOrg(
        errorhandler,
        function (resp) {
          $http.delete('/api/user/' + resp.data.data._id + '/' + user_id)
          .then(callback)
          .catch(errorhandler);
        });
      };

      // admin invites user
      userService.invite = function (userdata, errorhandler, callback) {
        $http.post('/api/user', userdata)
        .then(callback)
        .catch(errorhandler);
      };

      // user is confirming after having clicked on confirmlink
      userService.confirm = function (userdata, errorhandler, callback) {
        $http.post('/api/confirm', userdata)
        .then(callback)
        .catch(errorhandler);
      };

      // called when user confirm he/she has forgot password
      userService.recoverPassword = function (userdata, errorhandler, callback) {
        $http.post('/api/recoverpassword', userdata)
        .then(callback)
        .catch(errorhandler);
      };

      // called when user has pressed link in mail confirming he/she is the
      // correct user
      userService.resetPasswordAndLogin = function (userdata, errorhandler, callback) {
        $http.post('/api/resetandlogin', userdata)
        .then(callback)
        .catch(errorhandler);
      };

      userService.login = function (userdata, errorhandler, callback) {
        $http.post('/api/login', userdata)
        .then(callback)
        .catch(errorhandler);
      };

      userService.isLoggedIn = function() {
        if (authService.getToken())
          return true;
        else
          return false;
      };

      userService.getCurrentUser = function(errorhandler, resp) {
        $http.get('/api/me')
        .then(resp)
        .catch(errorhandler);
      };

      // clears out any sensitive information
      userService.logout = function() {
        authService.clearToken();
        $state.go('login');
      };

      return userService;
    }
  ]
);



angular.module('home.controllers.homectrl', [])

.controller('HomeCtrl', ['$scope', '$timeout', '$log', 'userService', 'categoryFactory', 'orgService', 'supplierFactory',
  function ($scope, $timeout, $log, userService, categoryFactory, orgService, supplierFactory) {
    
    //Get categories from factory
    $scope.categories = categoryFactory;

    //First get the organisation and store it in the scope and then get the users from that organisation.
    orgService.getCurrentOrg(
      function (err) { /* TODO: handle error somehow */ },
      function (resp) {
        if(resp.data.data.name) {
          $scope.org = resp.data.data.name;
          userService.all($scope.org,
            function (err) { /* TODO: should handle error */ },
            function (resp) {
              var data = resp.data.data;
              $scope.users = data;
            }
          );
        }
      }
    );

    userService.getCurrentUser(
      function (err) { /* TODO: handle error somehow */ },
      function (resp) {
        if(resp.data)
          $scope.user = resp.data;
      }
    );

    supplierFactory.get(); // get all suppliers
      
    $scope.$watch( // watch for new suppliers in supplierFactory
      function() {
        return supplierFactory.suppliers;
      },
      function (newval, oldval) {
        if(newval.length !== oldval.length) {
          $timeout(function() {
            $scope.suppliers = newval;
          }, false); // to avoid flickering, asynchronous trick :-)
        } else {
            $scope.suppliers = oldval;
        }
      }
    );
  }
]);

angular.module('modules.home', ['home.controllers.homectrl'])

.config(['$stateProvider', function($stateProvider) {

  $stateProvider.state('hem', {
    url: '/hem',
    views: {
      'main@': {
        controller: 'HomeCtrl',
        templateUrl: 'modules/home/partials/home.tpl.html'
      }
    },
    access: {
      type: "private",
      groups: "all"
    }
  });
}]);

angular.module('login.controllers.forgotpasswordctrl', [])

/*
 * This controller controls the forgotpassword modal
 */
.controller('ForgotPasswordCtrl',
  ['$scope', '$modalInstance', '$timeout', 'userService',
    function($scope, $modalInstance, $timeout, userService) {

      // To be filled in by the user through the form
      $scope.userdata = {};

      // User posts registration form
      $scope.createRecoveryLink = function(userdata) {
        userService.recoverPassword(userdata,
          // Something went wrong with connection
          function(err) {
            $scope.success = false;
            $scope.message = "Något gick fel. Försök igen och kontakta support om felet kvarstår.";
          },
          // success or user-mail is not registered
          function(resp) {
            var data = resp.data;
            $scope.success = data.success; // false or true
            $scope.message = data.message; // alert message
            if(data.success) { // close modal in 2 sec if successfull registration
              $timeout(function() {
                $modalInstance.close('success');
              }, 2000);
            }
          }
        );
      };

      // Closes modal if user cancels registration
      $scope.forgotPasswordDismiss = function() {
        $modalInstance.dismiss('cancel');
      };

    }
  ]
);

angular.module('login.controllers.loginctrl', [])

/*
 * This controller controls the loginform and the registration&password modal
 */
.controller('LoginCtrl',
  ['$rootScope', '$scope', '$log', '$state', '$modal', '$timeout', 'orgService', 'userService', 'authService',
    function ($rootScope, $scope, $log, $state, $modal, $timeout, orgService, userService, authService) {

      // User posts login form
      $scope.loginUser = function (user) {
        userService.login(user,
          function (err) {
            $scope.failure = true;
            $scope.message = "Något gick fel. Försök igen och kontakta support om felet kvarstår.";
          },
          function (resp) {
            var data = resp.data;
            if(!data.success) { // failure: (wrong password or email)
              $scope.failure = true;
              $scope.message = data.message; // pass message from server on to user
              $scope.user.password = '';
            } else { // success: store token, user and org
              var data = data.data;
              authService.setToken(data.token);
              
              //Get current user data to store role in rootScope
              userService.getCurrentUser(
                function (err) {
                  $scope.failure = true;
                  $scope.message = "Kunde inte hämta din användare";
                }, function (resp) {
                  var currentUser = resp.data;
                  if(!currentUser) { // no data retreived
                    $scope.failure = true;
                    $scope.message = data.message; // pass message from server on to user
                  } else {
                  $rootScope.userRole = currentUser.role; //Store user role in rootScope
                  $rootScope.userName = currentUser.name; //Store user name in rootScope
                  $rootScope.userId = currentUser.userid; //Store user ID in rootScope
                  }
                }
              );

              $state.go("hem");

            }
          }
        );
      };

      // Function for loading modal (registration)
      $scope.showRegistration = function () {
        // Modal for registration
        var regModal = $modal.open({
          templateUrl: 'modules/login/partials/registration-modal.tpl.html',
          controller: 'RegistrationCtrl'
        });
        // function for when result gets back from registration
        regModal.result.then(function (data) {
          $state.go("login");
        });
      };

      // Function for loading modal (registration)
      $scope.forgotPassword = function () {
        // Modal for registration
        var forgotPasswordModal = $modal.open({
          templateUrl: 'modules/login/partials/forgotpassword-modal.tpl.html',
          controller: 'ForgotPasswordCtrl'
        });
        // function for when result gets back from registration
        forgotPasswordModal.result.then(function (data) {
          $state.go("login");
        });
      };

    }
  ]
);

angular.module('login.controllers.registrationctrl', [])

/*
 * This controller controls the registrationform modal
 */
.controller('RegistrationCtrl',
  ['$scope', '$log', '$modalInstance', '$timeout', 'orgService',
    function ($scope, $log, $modalInstance, $timeout, orgService) {

      // To be filled in by the user through the form
      $scope.user = {};

      // Clear errors, reinsert data from user and mark appropriate errors
      $scope.reset = function() {
        $scope.regForm.$setPristine();
        $scope.user = $scope.copy; // put data back in form
        $scope.user.password = '';
        $scope.user.confirm = '';
      };

      // User posts registration form
      $scope.createOrg = function(user) {
        $scope.copy = angular.copy(user); // put data back in form later
        orgService.create(user,
          // Something went wrong with connection
          function (err) {
            $scope.success = false;
            $scope.message = "Något gick fel. Försök igen och kontakta support om felet kvarstår.";
            $scope.reset();
          },
          // success or user-mail is already registered, or incorrect password
          function (resp) {
            var data = resp.data;
            $scope.success = data.success; // false or true
            $scope.message = data.message; // alert message
            if(data.success) { // close modal in 2 sec if successfull registration
              $timeout(function() {
                $modalInstance.close('success');
              }, 2000);
            } else { // not successfull (org or mail was registered or password incorrect)
              $log.log(resp);
              $scope.reset();
            }
          }
        );
      };

      // Closes modal if user cancels registration
      $scope.regDismiss = function() {
        $modalInstance.dismiss('cancel');
      };

    }
  ]
);

angular.module('modules.login', [
        'common',
	      'login.controllers.loginctrl',
	      'login.controllers.registrationctrl',
        'login.controllers.forgotpasswordctrl'
	])

.config(['$stateProvider', function($stateProvider) {
  $stateProvider.state('login', {
    url: '/login',
    views: {
      'main@': {
        controller: 'LoginCtrl',
        templateUrl: 'modules/login/partials/login.tpl.html'
      }
    },
    access: {
      type: "public",
      groups: "all"
    }
  });
}]);

angular.module('navbar.controllers.navbarctrl', [])

/**
 * This controller controls the navbar.
 */
.controller('NavbarCtrl',
  ['$scope', '$state', 'userService', 'authService',
    function($scope, $state, userService, authService) {
      // User logout
      $scope.logoutUser = function() {
        userService.logout();
      };
    }
  ]
);

angular.module('modules.navbar', [
  'navbar.controllers.navbarctrl'
]);


angular.module('orgconfirmation.controllers.confirmationctrl', [])

.controller('ConfirmationCtrl',
  ['$scope', '$state', '$stateParams', '$timeout', 'orgService',
    function($scope, $state, $stateParams, $timeout, orgService) {
      var id   = $stateParams.id,
          hash = $stateParams.hash;

      orgService.confirm(id, hash,
        function(err) {
          $scope.success = false;
          $scope.message = "Något gick fel, vi kunde inte bekräfta din användare. Kontakta support.";
          $timeout(function() {
            $state.go('login');
          }, 3000);
        },
        function(resp) {
          var data = resp.data;
          $scope.success = data.success;
          $scope.message = data.message;
          $timeout(function() {
            $state.go('login');
          }, 3000);
        }
      );

    }
  ]
);

angular.module('modules.orgconfirmation', [
    'orgconfirmation.controllers.confirmationctrl'
    ])

.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

  $stateProvider.state('confirm', {
    url: '/confirm/:id/:hash',
    views: {
      'main@': {
        controller: 'ConfirmationCtrl',
        templateUrl: 'modules/orgconfirmation/partials/confirmation.tpl.html'
      }
    },
    access: {
      type: "public",
      groups: "all"
    }
  });

}]);

angular.module('resetpassword.controllers.resetpasswordctrl', [])

.controller('ResetPasswordCtrl',
  ['$scope', '$state', '$rootScope', '$log', '$stateParams', 'userService', 'authService',
    function ($scope, $state, $rootScope, $log, $stateParams, userService, authService) {

      $scope.resetPasswordAndLogin = function(password) {
        var userdata = {
          password: password,
          orgid: $stateParams.orgid,
          userid: $stateParams.userid,
          hash: $stateParams.hash
        };
        userService.resetPasswordAndLogin(userdata,
          function (err) {
            $scope.failure = true;
            $scope.message = "Något gick fel. Försök igen och kontakta support om felet kvarstår.";
          },
          function (resp) {
            var data = resp.data;
            if(!data.success) { // failure: (wrong password or email)
              $scope.failure = true;
              $scope.message = data.message; // pass message from server on to user
              $scope.password = '';
              $scope.confirm = '';
            } else { // success (login): store token, user and org
              authService.setToken(data.data.token);
              $state.go("hem");
            }
          }
        );
      }

    }
  ]
);

angular.module('modules.resetpassword', [
  'common',
  'resetpassword.controllers.resetpasswordctrl'
])

.config(['$stateProvider', function($stateProvider) {

  $stateProvider.state('reset-password', {
    url: '/reset-password/:orgid/:userid/:hash',
    views: {
      'main@': {
        controller: 'ResetPasswordCtrl',
        templateUrl: 'modules/resetpassword/partials/reset-password.tpl.html'
      }
    },
    access: {
      type: "public",
      groups: "all"
    }
  });

}]);

angular.module('suppliers.controllers.singlesupplierctrl', [])

.controller('SingleSupplierCtrl', ['$scope', '$rootScope', '$timeout', '$log', '$state', 'supplierFactory', '$stateParams', 'categoryFactory', 'segmentFactory',
  function ($scope, $rootScope, $timeout, $log, $state, supplierFactory, $stateParams, categoryFactory, segmentFactory) {

    $scope.letterLimit = 120;
    $scope.letterLimitHeading = 30;
    $scope.suppliers = [];
    $scope.selectedSupplier = [];
    $scope.categories = categoryFactory;
    $scope.segments = segmentFactory;
    $scope.editing = false;
    $scope.alerts = [];

    var setBreadcrumbs = function () {
      $rootScope.firstStateTitle = 'Leverantörer';
      $rootScope.secondStateTitle = $scope.selectedSupplier.name;
    };

    //Clickhandler for breadcrumb state change
    $rootScope.clickHandler = function (param){
      $state.go('leverantorer');
    };

    $scope.setEditing = function (state) {
      $scope.editing = state;
    };

    //Closes the alert bow
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };

    // get all suppliers
    supplierFactory.get(); 

    // watch for new suppliers in supplierFactory
    $scope.$watch( 
      function() {
        return supplierFactory.suppliers;
      },
      function (newval, oldval) {
        if(newval.length !== oldval.length) {
          $timeout(function() {
            $scope.suppliers = newval;
          }, false); // to avoid flickering, asynchronous trick :-)
        } else {
            $scope.suppliers = oldval;
        }
      }
    );

    // Get one supplier from state params and ID (immediately executed)
    $scope.getSingleSupplier = function () {
      supplierFactory.getOne($stateParams.supplier_id,
        function (err) { /* TODO: should handle error somehow */ },
        function (resp) {
          if(resp.data.success) {
            $scope.selectedSupplier = resp.data.data;
            setBreadcrumbs();
          }
        }
      );
    };
    

    //Called when aborting the editing form
    $scope.reset = function (form) {
      $scope.editing = false;
      $scope.getSingleSupplier(); //Get the single supplier again
      if (form) {
        form.$setPristine();
        form.$setUntouched();
      }
    };

    //Called from ngClick when submitting the editing form
    $scope.submit = function($event, form, supplier) {
      //Calls factory to update supplier from form data
      supplierFactory.update($stateParams.supplier_id, supplier,
        function (err) { // errorhandler
          //Push a new alert
          $scope.alerts.push({
            msg: 'Kunde inte uppdatera' + supplier.name, 
            type: 'danger'
          });
        },
        function (resp) { // onsuccess
          var data = resp;
          if (data.data) {
            //Push a new alert
            var alert = {};
            alert.msg = data.data.message;
            if (data.data.success) {
              alert.type = 'success';
            } else {
              alert.type = 'danger';
            }
            $scope.alerts.push({
              msg: alert.msg, 
              type: alert.type
            });
          //$scope.supplierCopy = data.config.data;
          $scope.editing = false;
          form.$setPristine();
          form.$setUntouched();
          }
        }
      );
    };

    $scope.deleteSupplier = function () {
      supplierFactory.delete($stateParams.supplier_id,
        function (err) { /* TODO: should handle error somehow */ },
        function (resp) {
          $state.go("leverantorer");
        }
      );
    }


  // $scope.itemsPerPage = 4;
  // $scope.currentPage = 1;
  // $scope.pageCount = function () {
  //   console.log(Math.ceil($scope.suppliers.length / $scope.itemsPerPage));
  //   return Math.ceil($scope.suppliers.length / $scope.itemsPerPage);
  // };
  // $scope.suppliers.$promise.then(function () {
  //   $scope.totalItems = $scope.suppliers.length;
  //   $scope.$watch('currentPage + itemsPerPage', function() {
  //     var begin = (($scope.currentPage - 1) * $scope.itemsPerPage),
  //       end = begin + $scope.itemsPerPage;
  //     $scope.filteredFriends = $scope.friends.slice(begin, end);
  //     });
  //   });
  }
]);

angular.module('suppliers.controllers.supplierformctrl', [])

.controller('SupplierFormCtrl', ['$log', '$timeout', '$upload', '$scope', 'supplierFactory',
  function ($log, $timeout, $upload, $scope, supplierFactory) {

    //$scope.categories = categoryFactory;
    //$scope.segments = segmentFactory;
    //$scope.pagination = 5;
    $scope.supplier = {};
    $scope.supplierCopy = {};
    $scope.success = false;
    $scope.failure = false;

    // when supplierCopy gets set after submitting data update supplierFactory
    $scope.$watch('supplierCopy', function (newval, oldval) {
      supplierFactory.suppliers.push(newval);
    });

    $scope.createSupplier = function (supplier) {
      supplierFactory.create(supplier,
        function (err) { // errorhandler
          $scope.failure = true;
          $scope.submitMessage = err;
        },
        function (resp) { // onsuccess
          var data = resp.data;
          if (data.success) {
            $scope.success = true;
            $scope.supplierCopy = data.data;
          } else {
            $scope.failure = true;
          }
          $scope.submitMessage = data.message;
          $scope.supplier = {}; // clear data out
        }
      );
    };

    // $scope.usingFlash = FileAPI && FileAPI.upload != null;
    // $scope.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);

    // $scope.$watch('files', function (files) {
    //   $scope.formUpload = false;
    //   if (files != null) {
    //     for (var i = 0; i < files.length; i++) {
    //       $scope.errorMsg = null;
    //       (function (file) {
    //         generateThumbAndUpload(file);
    //       })(files[i]);
    //     }
    //   }
    // });

    // $scope.uploadPic = function(files) {
    //   $scope.formUpload = true;
    //   if (files != null) {
    //     generateThumbAndUpload(files[0])
    //   }
    // };

    // function generateThumbAndUpload(file) {
    //   $scope.errorMsg = null;
    //   $scope.generateThumb(file);
    //   uploadUsing$upload(file);
    // }

    // $scope.generateThumb = function(file) {
    //   if (file != null) {
    //     if ($scope.fileReaderSupported && file.type.indexOf('image') > -1) {
    //       $timeout(function () {
    //         var fileReader = new FileReader();
    //         fileReader.readAsDataURL(file);
    //         fileReader.onload = function(e) {
    //           $timeout(function() {
    //             file.dataUrl = e.target.result;
    //           });
    //         }
    //       });
    //     }
    //   }
    // };

    // function uploadUsing$upload(file) {
    //   file.upload = $upload.upload({
    //     url: 'api/upload' + $scope.getReqParams(),
    //     method: 'POST',
    //     headers: {
    //       'my-header' : 'my-header-value' // TODO: is this necessary?
    //     },
    //     fields: {username: $scope.username}, // TODO: change to use userservice.getCurrentUser?
    //     file: file,
    //     fileFormDataName: 'myFile'
    //   });

    //   file.upload.then(function (response) {
    //     $timeout(function () {
    //       file.result = response.data;
    //     });
    //   }, function (response) {
    //     if (response.status > 0)
    //       $scope.errorMsg = response.status + ': ' + response.data;
    //   });

    //   file.upload.progress(function (evt) {
    //     // Math.min is to fix IE which reports 200% sometimes
    //     file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
    //   });

    //   file.upload.xhr(function (xhr) {
    //     // xhr.upload.addEventListener('abort', function(){console.log('abort complete')}, false);
    //   });
    // }

    // $scope.getReqParams = function () {
    //   return $scope.generateErrorOnServer ? '?errorCode=' + $scope.serverErrorCode +
    //     '&errorMessage=' + $scope.serverErrorMsg : '';
    // };

  }
]);

angular.module('suppliers.controllers.suppliersctrl', [])

.controller('SuppliersCtrl',
  ['$scope', '$rootScope', '$timeout', '$log', '$stateParams', 'supplierFactory', 'categoryFactory', 'segmentFactory',
    function ($scope, $rootScope, $timeout, $log, $stateParams, supplierFactory, categoryFactory, segmentFactory) {

      $scope.categories = categoryFactory;
      $scope.segments = segmentFactory;
      $scope.suppliers = [];

      //Set filter from URL params
      if ($stateParams.filterParam) {
        console.log($stateParams.filterParam);
        $scope.filter = $stateParams.filterParam;
      } else {
        //Filter variable to hold bolean values for properties
        $scope.filter = { };
      }

      //Set breadcrumb
      $rootScope.firstStateTitle = 'Leverantörer';

      

      //scope variable for drop-down filter
      $scope.selectedSegment = [];

      $scope.letterLimit = 90;
      $scope.letterLimitHeading = 30;
      $scope.showForm = false;

      supplierFactory.get(); // get all suppliers
      
      $scope.$watch( // watch for new suppliers in supplierFactory
        function() {
          return supplierFactory.suppliers;
        },
        function (newval, oldval) {
          if(newval.length !== oldval.length) {
            $timeout(function() {
              $scope.suppliers = newval;
            }, false); // to avoid flickering, asynchronous trick :-)
          } else {
              $scope.suppliers = oldval;
          }
        }
      );

      // //List filter segment
      // $scope.setSelectedSegment = function () {
      //   var id = this.segment.id;
      //   if (_.contains($scope.selectedSegment, id)) {
      //     $scope.selectedSegment = _.without($scope.selectedSegment, id);
      //   } else {
      //     $scope.selectedSegment.push(id);
      //   }
      //   return false;
      // };

      // $scope.isChecked = function (id) {
      //   if (_.contains($scope.selectedSegment, id)) {
      //     return 'glyphicon glyphicon-ok pull-right';
      //   }
      //   return false;
      // };

      // $scope.checkAll = function () {
      //   $scope.selectedSegment = _.pluck($scope.segments, 'id');
      // };

      //Get the currently used segments from suppliers scope
      $scope.getSegments = function () {
        return ($scope.suppliers || []).map(function (supplier) {
          return supplier.segment;
        }).filter(function (supplier, index, arr) {
          return arr.indexOf(supplier) === index;
        });
      };

      //Get the currently used categories from suppliers scope
      $scope.getCategories = function () {
        return ($scope.suppliers || []).map(function (supplier) {
          return supplier.category;
        }).filter(function (supplier, index, arr) {
          return arr.indexOf(supplier) === index;
        });
      };


      //Filter to handle multiple properties
      $scope.filterByProperties = function (supplier) {
        // Use this snippet for matching with AND
        var matchesAND = true;
        for (var prop in $scope.filter) {
          if (noSubFilter($scope.filter[prop])) continue;
          if (!$scope.filter[prop][supplier[prop]]) {
            matchesAND = false;
            break;
          }
        }
        return matchesAND;
/**/
/*
        // Use this snippet for matching with OR
        var matchesOR = true;
        for (var prop in $scope.filter) {
            if (noSubFilter($scope.filter[prop])) continue;
            if (!$scope.filter[prop][wine[prop]]) {
                matchesOR = false;
            } else {
                matchesOR = true;
                break;
            }
        }
        return matchesOR;
/**/
      };
      
      function noSubFilter(subFilterObj) {
        for (var key in subFilterObj) {
          if (subFilterObj[key]) return false;
        }
        return true;
      }
      //End controller
    }
  ]
);

angular.module('modules.suppliers', [
    'common',
    'suppliers.controllers.suppliersctrl',
    'suppliers.controllers.singlesupplierctrl',
    'suppliers.controllers.supplierformctrl'
 ])

.config(['$stateProvider', function($stateProvider) {

  $stateProvider.state('leverantorer', {
    url: '/leverantorer/:filterParam',
    views: {
      "main@": {
        controller: 'SuppliersCtrl',
        templateUrl: 'modules/suppliers/partials/suppliers.tpl.html'
      },
      "form@leverantorer": {
        controller: 'SupplierFormCtrl',
        templateUrl: 'modules/suppliers/partials/supplier-form.tpl.html',
      }
    },
    access: {
      type: "private",
      groups: "all"
    }
  })

  .state('leverantorer.leverantor', {
    url: '/:supplier_id',
    views: {
      "main@": {
        controller: 'SingleSupplierCtrl',
        templateUrl: 'modules/suppliers/partials/single-supplier.tpl.html',
      }
    },
    access: {
      type: "private",
      groups: "all"
    }
  })

  ;
}])

.directive('supplierLabels', function() {
  return {
    restrict: 'E',
    templateUrl: 'modules/suppliers/partials/supplier-labels.tpl.html',
    scope: {
      category: '=',
      segment: '='
    }
  }
});

// .directive('supplierForm', function(){
//     return {
//       restrict: 'E',
//       templateUrl: 'modules/suppliers/partials/supplier-form.tpl.html',
//       replace: true,
//       controller: 'SupplierFormCtrl as form',
//       scope: false
//       // scope: {
//       //   category: '=',
//       //   segment: '='
//       // }
//     }
//   })


// //Makes breadcrumb resizeable
// $(document).ready(function(){
//     $(window).resize(function() {
//         ellipses1 = $("#bc1 :nth-child(2)")
//         if ($("#bc1 a:hidden").length >0) {ellipses1.show()} else {ellipses1.hide()}
//     })
// });

angular.module('useradministration.controllers.invitationmodalinstancectrl', [])

.controller('InvitationModalInstanceCtrl',
  ['$log', '$modalInstance', '$scope', '$timeout',  'userService',
    function ($log, $modalInstance, $scope, $timeout, userService) {
      
      $scope.candidate = {};
      $scope.candidate.role = 'viewer';
      $scope.mailtext = '';
      $scope.explanation = "Välj rättigheter för användaren";

      $scope.$watch('candidate.role', function (value) {
        switch(value) {
          case("admin"):
            $scope.explanation = "Admin (användarredigering, leverantörsredigering, leverantörsbevakning)";
            break;
          case("editor"):
            $scope.explanation = "Editor (leverantörsredigering, leverantörsbevakning)";
            break;
          case("viewer"):
            $scope.explanation = "Viewer (leverenatörsbevakning)";
            break;
          default:
            $scope.explanation = "Välj rättigheter för användaren.";
        }
      });

      // Clear form
      $scope.reset = function() {
        $scope.inviteForm.$setPristine();
      };

      $scope.inviteUser = function (candidate, mailtext) {
        var userdata = {
          candidate: candidate,
          mailtext: mailtext
        };
        console.log(userdata);
        userService.invite(userdata,
          function (err) {
            $scope.success = false;
            $scope.message = "Något gick fel vid inbjudan. Kontakta support.";
            $scope.reset();
          },
          function (resp) {
            var data = resp.data;
            $scope.success = data.success;
            $scope.message = data.message;
            if(data.success) { // close modal in 2 sec if successfull registration
              $timeout(function() {
                $modalInstance.close('success');
              }, 2000);
            } else { // not successfull (org or mail was registered or password incorrect)
              $scope.reset();
            }
          }
        );
      };

      // Closes modal if user cancels invitation
      $scope.dismiss = function() {
        $modalInstance.dismiss('cancel');
      };
      //End controller
    }
  ]
);

angular.module('useradministration.controllers.invitationctrl', [])

.controller('InvitationCtrl',
  ['$log', '$scope', '$state', '$stateParams', '$timeout', 'userService',
    function ($log, $scope, $state, $stateParams, $timeout, userService) {
      $scope.candidate = {};

      $scope.explanation = "Välj rättigheter för användaren";
      $scope.$watch('candidate.role', function (value) {
        switch(value) {
          case("admin"):
            $scope.explanation = "Admin (användarredigering, leverantörsredigering, leverantörsbevakning)";
            break;
          case("editor"):
            $scope.explanation = "Editor (leverantörsredigering, leverantörsbevakning)";
            break;
          case("viewer"):
            $scope.explanation = "Viewer (leverenatörsbevakning)";
            break;
          default:
            $scope.explanation = "Välj rättigheter för användaren.";
        }
      });

      $scope.inviteUser = function (candidate, mailtext) {
        var userdata = {
          candidate: candidate,
          mailtext: mailtext
        };
        userService.invite(userdata,
          function (err) {
            $scope.success = false;
            $scope.message = "Något gick fel vid inbjudan. Kontakta support.";
          },
          function (resp) {
            var data = resp.data;
            $scope.success = data.success;
            $scope.message = data.message;
          }
        );
      };

    }
  ]
);

angular.module('useradministration.controllers.removeusermodalctrl', [])

.controller('RemoveUserModalCtrl',
  ['$log', '$modalInstance', '$scope', '$timeout', 'user',
    function ($log, $modalInstance, $scope, $timeout, user) {
      
      $scope.user = user;

      $scope.ok = function () {
        $modalInstance.close(true);
      };

      // Closes modal if user cancels registration
      $scope.dismiss = function() {
        $modalInstance.dismiss('cancel');
      };
      //End controller
    }
  ]
);

angular.module('useradministration.controllers.singlesuserctrl', [])

.controller('SingleUserCtrl', ['$scope', '$rootScope', '$timeout', '$log', '$state', 'userService', '$stateParams',
  function ($scope, $rootScope, $timeout, $log, $state, userService, $stateParams) {

    $scope.letterLimit = 120;
    $scope.letterLimitHeading = 30;

    var setBreadcrumbs = function () {
      $rootScope.firstStateTitle = 'Användaradministration';
      $rootScope.secondStateTitle = $scope.selectedUser.name;
    };

    //Clickhandler for breadcrumb state change
    $rootScope.clickHandler = function (param){
      $state.go('anvandare');
    }

    // Get one user from state params and ID (immediately executed)
    userService.getOne($stateParams.user_id,
      function (err) { 
        console.log('Error in getting a user' + err);
      },
      function (resp) {
        if(resp.data)
          //Load response data into the scope
          $scope.selectedUser = resp.data.data;

          //Variables for breadcrumb in header
          setBreadcrumbs();
      }
    );

    //Delete user by user_id from params
    $scope.deleteUser = function (user_id) {
      userService.delete($stateParams.user_id,
        function (err) { /* TODO: should handle error somehow */ },
        function (resp) {
          $state.go("anvandare");
        }
      );
    }
    //End controller
  }
]);

angular.module('useradministration.controllers.userregistrationctrl', [])

/*
 * This controller controls the registrationform modal
 */
.controller('UserRegistrationCtrl',
  ['$scope', '$log', '$state', '$stateParams', '$timeout', 'userService',
    function($scope, $log, $state, $stateParams, $timeout, userService) {

      // To be filled in by the user through the form
      $scope.user = {};

      // Clear errors, reinsert data from user and mark appropriate errors
      $scope.reset = function() {
        $scope.regForm.$setPristine();
        $scope.user = $scope.copy; // put data back in form
        $scope.user.password = '';
        $scope.user.confirm = '';
      };

      // User posts registration form
      $scope.registerUser = function(user) {
        $scope.copy = angular.copy(user); // put data back in form later
        userService.confirm({
            userdata: user,
            orgid: $stateParams.orgid,
            candid: $stateParams.candid,
            hash: $stateParams.hash
          },
          // Something went wrong with connection
          function (err) {
            $scope.success = false;
            $scope.message = "Något gick fel. Försök igen och kontakta support om felet kvarstår.";
          },
          // success or user-mail is already registered, or incorrect password
          function (resp) {
            $log.log(resp);
            var data = resp.data;
            $scope.success = data.success; // false or true
            $scope.message = data.message; // alert message
            if(data.success) { // close modal in 2 sec if successfull registration
              $timeout(function() {
                $state.go('login');
              }, 3000);
            } else { // not successfull (org or mail was registered or password incorrect)
              $scope.reset();
            }
          }
        );
      };

    }
  ]
);

angular.module('useradministration.controllers.userctrl', [])

.controller('UserCtrl', ['$scope', '$state', '$modal', '$rootScope', '$timeout', 'userService', 'supplierFactory', 'orgService',
    function ($scope, $state, $modal, $rootScope, $timeout, userService, supplierFactory, orgService) {

      $scope.message = {};
      $scope.users = [];
      $scope.org = '';
      $scope.sortType     = 'name'; // set the default sort type
      $scope.sortReverse  = false;  // set the default sort order
      $rootScope.firstStateTitle = 'Användaradministration';
      $scope.alerts = [];

      //First get the organisation and store it in the scope and then get the users from that organisation.
      orgService.getCurrentOrg(
        function (err) { 
        /* TODO: handle error and push an alert */ 
        },
        function (resp) {
          if(resp.data.data.name) {
            var org = resp.data.data.name;
            userService.all(org,
              function (err) { 
              /* TODO: should handle error */ 
              },
              function (resp) {
                $scope.users = resp.data.data;
                console.log($scope.users);
              }
            );
          }
        }
      );

      //Closes the alert bow
      $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
      };

      //Delete user by user_id
      $scope.deleteUser = function (user_id) {
        userService.delete(user_id,
          function (err) { 
            //Push a new alert
            $scope.alerts.push({
              msg: 'Tusan! Kunde inte ta bort användaren. :(', 
              type: 'danger'
            });
          },
          function (resp) {
            var data = resp.data;

            //Push a new alert
            var alert = {};
            console.log(data);
            alert.msg = data.message;
            if (data.success) {
              alert.type = 'success';
            } else {
              alert.type = 'danger';
            };
            $scope.alerts.push({
              msg: alert.msg, 
              type: alert.type
            })

            //Remove the deleted user from the scope
            for(var i = $scope.users.length - 1; i >= 0; i--){
              if($scope.users[i]._id == user_id){
                  console.log($scope.users[i]._id);
                  $scope.users.splice(i,1);
              }
            } //End for-loop

          } 
        ); //End userService.delete()
      }; //End scope.deleteUser()

      $scope.openRemoveModal = function (user) {

        var modalInstance = $modal.open({
          templateUrl: 'modules/useradministration/partials/removeuser-modal.tpl.html',
          controller: 'RemoveUserModalCtrl',
          size: 'sm',
          resolve: {
            user: function () {
              return user;
            }
          }
        });

        modalInstance.result.then(function (clickedOk) {
          console.log(clickedOk);
          console.log(user._id);
          if(clickedOk) {
            $scope.deleteUser(user._id);
          };
        }, function () {
          //$log.info('Modal dismissed at: ' + new Date());
        });
      };

      //Function for opening the invitation modal
      $scope.openInviteModal = function () {
        var inviteModal = $modal.open({
          templateUrl: 'modules/useradministration/partials/invitation-modal.tpl.html',
          controller: 'InvitationModalInstanceCtrl'
        });
        inviteModal.result.then(function (data) {
          $state.go("anvandare");
        });
      };

    //End controller
    }
  ]
);


angular.module('modules.useradministration', [
    'useradministration.controllers.invitationctrl',
    'useradministration.controllers.invitationmodalinstancectrl',
    'useradministration.controllers.removeusermodalctrl',
    'common',
    'useradministration.controllers.userctrl',
    'useradministration.controllers.userregistrationctrl',
    'useradministration.controllers.singlesuserctrl',
    ])

.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

  $stateProvider.state('inbjudan', {
    url: '/inbjudan',
    views: {
      'main@': {
        controller: 'InvitationCtrl',
        templateUrl: 'modules/useradministration/partials/invitation.tpl.html'
      }
    },
    access: {
      type: "private",
      groups: "admin"
    }
  });

    $stateProvider.state('anvandare', {
    url: '/anvandare',
    views: {
      'main': {
        controller: 'UserCtrl',
        templateUrl: 'modules/useradministration/partials/users.tpl.html'
      }
    },
    access: {
      type: "private",
      groups: "all"
    }
  });

  $stateProvider.state('invitationconfirmation', {
    url: '/invitationconfirmation/:orgid/:candid/:hash',
    views: {
      'main': {
        controller: 'UserRegistrationCtrl',
        templateUrl: 'modules/useradministration/partials/user-registration.tpl.html'
      }
    },
    access: {
      type: "public",
      groups: "all"
    }
  });

  $stateProvider.state('anvandare.enanvandare', {
    url: '/:user_id',
    views: {
      "main@": {
        controller: 'SingleUserCtrl',
        templateUrl: 'modules/useradministration/partials/single-user.tpl.html',
      }
    },
    access: {
      type: "private",
      groups: "all"
    }
  })

}]);

angular.module('templates-main', []).run(['$templateCache', function($templateCache) {
  $templateCache.put("modules/home/partials/home.tpl.html",
    "<div class=container-fluid><div class=row><div class=\"admincards col-xs-12 col-sm-10 col-sm-offset-1\"><div class=\"col-xs-12 col-md-6 col-lg-4\"><div class=\"card darkwind text-center\"><h2><span class=\"glyphicon glyphicon-user\" aria-hidden=true></span><small>&nbsp;&nbsp;&nbsp;Antal användare:</small> {{users.length}}</h2><a ui-sref=anvandare><button class=\"btn btn-grey\">->användare</button></a></div></div><div class=\"col-xs-12 col-md-6 col-lg-4\"><div class=\"card oviu-lightgrey text-center\"><h2><span class=\"glyphicon icon-Leverantor\" aria-hidden=true></span><small>&nbsp;&nbsp;&nbsp;Antal leverantörer:</small> {{suppliers.length}}</h2><a ui-sref=leverantorer><button class=\"btn btn-grey\">->leverantörer</button></a></div></div><div class=\"col-xs-12 col-md-6 col-lg-4\"><div class=\"card wind text-center\"><h2><span class=\"glyphicon glyphicon-list-alt\" aria-hidden=true></span><small>&nbsp;&nbsp;&nbsp;Antal kategorier:</small> {{categories.length}}</h2></div></div></div></div><div class=\"row row-offcanvas row-offcanvas-right\"><div class=\"col-xs-12 col-sm-10 col-md-8 col-sm-offset-1 col-md-offset-2\"><div class=\"jumbotron text-center\"><h1>Hej {{user.name}}!</h1><br><p id=welcome>Välkommen till</p><div class=row><div class=\"col-sm-4 col-xs-6 col-sm-offset-4 col-xs-offset-3\"><img src=http://suppliercloud.se/wp-content/uploads/2014/11/O-viu_logotplus_bluelight_rgbRETINA.png class=img-responsive alt=O-viu></div></div><br><p>Du tillhör organisationen {{org}}</p></div></div></div></div>");
  $templateCache.put("modules/login/partials/forgotpassword-modal.tpl.html",
    "<div class=modal tabindex=-1 role=dialog><div class=\"modal-dialog modal-md\"><div class=modal-content><div class=\"modal-header text-center\"><h4 class=\"modal-title authform-header-content\">Vi skickar instruktioner för återställning till dig</h4><span type=button class=\"glyphicon glyphicon-remove pull-right authform-header-content\" id=authform-close data-dismiss=modal ng-click=forgotPasswordDismiss()></span></div><div class=modal-body><form ng-submit=createRecoveryLink(userdata) name=recoverForm novalidate><div class=\"alert alert-success\" role=alert ng-show=\"success &&\n" +
    "            message\">{{message}}</div><div class=\"alert alert-danger\" role=alert ng-show=\"!success &&\n" +
    "            message\">{{message}}</div><div class=form-group ng-class=\"{ 'has-error' :\n" +
    "            recoverForm.orgName.$invalid && recoverForm.$dirty }\"><div class=input-group><span class=input-group-addon><i class=\"glyphicon glyphicon-briefcase\"></i></span> <input class=form-control id=org-name name=orgName ng-model=userdata.orgname required placeholder=Företagsnamn></div><p class=help-block ng-show=\"recoverForm.orgName.$invalid &&\n" +
    "            recoverForm.orgName.$dirty\"><span>Du glömde ange företagsnamnet.</span></p></div><div class=form-group ng-class=\"{ 'has-error' : (recoverForm.userEmail.$invalid &&\n" +
    "            recoverForm.userEmail.$dirty) || (recoverForm.$submitted && !success) }\"><div class=input-group><span class=input-group-addon>@</span> <input type=email class=form-control id=user-email name=userEmail ng-model=userdata.email required placeholder=E-mail></div><p class=help-block ng-show=mailerror><span>Denna mailaddress är inte registrerad.</span></p></div><div class=\"modal-footer text-center\"><button type=submit class=\"btn btn-block btn-lg btn-wind\">Återställ lösenord</button></div></form></div></div></div></div>");
  $templateCache.put("modules/login/partials/login.tpl.html",
    "<div class=container style=margin-top:40px><div class=row><div class=\"col-sm-6 col-md-4 col-md-offset-4\"><div class=\"panel panel-default\"><div class=panel-heading><strong>Logga in för att komma åt webbappen.</strong></div><div class=panel-body><form class=form-signin ng-submit=loginUser(user) name=loginForm novalidate><div class=\"alert alert-danger\" role=alert ng-show=\"failure && message\">{{message}}</div><fieldset><div class=row><div class=center-block><img class=\"profile-img img-responsive\" src=http://suppliercloud.se/wp-content/uploads/2014/11/O-viu_logotplus_bluelight_rgbRETINA.png alt=\"\"></div></div><div class=row><div class=\"col-sm-12 col-md-10 col-md-offset-1\"><div class=form-group><div class=input-group><span class=input-group-addon><i class=\"glyphicon glyphicon-briefcase\"></i></span> <input class=form-control id=org-name name=orgName ng-model=user.orgname placeholder=företagsnamn required autofocus></div></div><div class=form-group><div class=input-group><span class=input-group-addon>@</span> <input class=form-control id=user-email name=userEmail ng-model=user.email placeholder=namn@företag.se required autofocus></div></div><div class=form-group><div class=input-group><span class=input-group-addon><i class=\"glyphicon glyphicon-lock\"></i></span> <input type=password class=\"form-control col-xs-6\" id=user-password name=userPassword ng-model=user.password placeholder=Lösenord required></div></div><div class=form-group><input type=submit class=\"btn btn-lg btn-wind btn-block\" value=\"Logga in\"></div></div></div></fieldset></form></div><div class=panel-footer><div>Har du inget konto?&nbsp; <a href ng-click=showRegistration()>Registrera ditt företag här!</a><div><div>Har du&nbsp; <a href ng-click=forgotPassword()>glömt ditt lösenord?</a><div></div></div></div></div></div></div></div></div></div>");
  $templateCache.put("modules/login/partials/registration-modal.tpl.html",
    "<div class=modal tabindex=-1 role=dialog><div class=\"modal-dialog modal-md\"><div class=modal-content><div class=\"modal-header text-center\"><h4 class=\"modal-title authform-header-content\">Registrera ditt företag</h4><span type=button class=\"glyphicon glyphicon-remove pull-right authform-header-content\" id=authform-close data-dismiss=modal ng-click=regDismiss()></span></div><div class=modal-body><form ng-submit=createOrg(user) name=regForm novalidate><div class=\"alert alert-success\" role=alert ng-show=\"success &&\n" +
    "            message\">{{message}}</div><div class=\"alert alert-danger\" role=alert ng-show=\"!success &&\n" +
    "            message\">{{message}}</div><div class=form-group ng-class=\"{ 'has-error' : regForm.orgName.$invalid && regForm.$dirty }\"><div class=input-group><span class=input-group-addon><i class=\"glyphicon glyphicon-briefcase\"></i></span> <input class=form-control id=org-name name=orgName ng-model=user.orgname required placeholder=Företagsnamn></div><p class=help-block ng-show=\"regForm.orgName.$invalid &&\n" +
    "            regForm.orgName.$dirty\"><span>Du glömde ange företagsnamnet.</span></p></div><div class=form-group ng-class=\"{ 'has-error' :\n" +
    "            regForm.userName.$invalid && regForm.userName.$dirty }\"><div class=input-group><span class=input-group-addon><i class=\"glyphicon glyphicon-user\"></i></span> <input class=form-control id=user-name name=userName ng-model=user.name required placeholder=\"Ditt namn\"></div><p class=help-block ng-show=\"regForm.userName.$invalid && regForm.userName.$dirty\"><span>Du glömde ange ditt namn.</span></p></div><div class=form-group ng-class=\"{ 'has-error' : (regForm.userEmail.$invalid &&\n" +
    "            regForm.userEmail.$dirty) || (regForm.$submitted && !success) }\"><div class=input-group><span class=input-group-addon>@</span> <input type=email class=form-control id=user-email name=userEmail ng-model=user.email required autofocus placeholder=namn@företag.se></div><p class=help-block ng-show=\"regForm.userEmail.$invalid &&\n" +
    "              regForm.userEmail.$dirty\"><span>Du måste ange en giltig epostadress.</span></p><p class=help-block ng-show=mailerror><span>Denna mail adress är redan registrerad.</span></p></div><div class=form-group ng-class=\"{ 'has-error' : regForm.userPassword.$invalid &&\n" +
    "            regForm.userPassword.$dirty || passworderror }\"><div class=input-group><span class=input-group-addon><i class=\"glyphicon glyphicon-lock\"></i></span> <input type=password class=form-control id=user-password name=userPassword ng-model=user.password ng-minlength=8 required placeholder=Lösenord></div><p class=help-block ng-show=\"regForm.userPassword.$invalid &&\n" +
    "              regForm.userPassword.$dirty || passworderror\"><span>Ditt lösenord måste innehålla minst 8 tecken.</span></p></div><div class=form-group ng-class=\"{ 'has-error' : regForm.userConfirm.$invalid &&\n" +
    "            regForm.userConfirm.$dirty }\"><div class=input-group><span class=input-group-addon>x2</span> <input type=password class=form-control id=user-confirm name=userConfirm ng-model=user.confirm password-verify=user.password required placeholder=\"Repetera lösenord\"></div><p class=help-block ng-show=regForm.userConfirm.$error.passwordVerify><span>Du angav inte samma lösenord som ovan.</span></p></div><div class=\"modal-footer text-center\"><button ng-disabled=regForm.$invalid type=submit class=\"btn btn-block btn-lg btn-wind\">Registrera</button></div></form></div></div></div></div>");
  $templateCache.put("modules/navbar/partials/login-modal.tpl.html",
    "<div class=modal tabindex=-1 role=dialog><div class=modal-dialog><div class=modal-content><div class=modal-header><h4 class=\"modal-title authform-header-content\">Login</h4><span type=button class=\"glyphicon glyphicon-remove pull-right authform-header-content\" id=authform-close data-dismiss=modal ng-click=loginDismiss()></span></div><div class=modal-body><form ng-submit=loginUser(user) name=loginForm novalidate><div class=\"alert alert-danger\" role=alert ng-show=\"failure && message\">{{message}}</div><div class=form-group><label for=user-email class=control-label>Email:</label><input class=form-control id=user-email name=userEmail ng-model=user.email placeholder=\"Din mail\"></div><div class=form-group><label for=user-password class=control-label>Lösenord:</label><input type=password class=form-control id=user-password name=userPassword ng-model=user.password placeholder=\"Ditt lösenord\"></div><button type=submit class=\"btn btn-primary\">Logga in</button></form></div><div class=modal-footer><button type=button class=\"btn btn-default\" data-dismiss=modal ng-click=loginDismiss()>Stäng</button></div></div></div></div>");
  $templateCache.put("modules/orgconfirmation/partials/confirmation.tpl.html",
    "<div class=container-fluid><div class=\"row row-offcanvas row-offcanvas-right\"><div class=\"col-xs-12 col-sm-10 col-sm-offset-1\"><div class=\"alert alert-success\" role=alert ng-show=success>{{message}}</div><div class=confirmation-box ng-show=success>Du kan nu logga in!</div><div class=\"alert alert-danger\" role=alert ng-show=!success>{{message}}</div><div class=confirmation-box ng-show=!success>Något gick fel i bekräftelsen!</div></div></div></div>");
  $templateCache.put("modules/resetpassword/partials/reset-password.tpl.html",
    "<div class=container style=margin-top:40px><div class=row><div class=\"col-sm-6 col-md-4 col-md-offset-4\"><div class=\"panel panel-default\"><div class=panel-heading><strong>Återställ ditt lösenord</strong></div><div class=panel-body><form class=form-signin ng-submit=resetPasswordAndLogin(password) name=resetForm novalidate><div class=\"alert alert-danger\" role=alert ng-show=\"failure &&\n" +
    "              message\">{{message}}</div><fieldset><div class=row><div class=\"col-sm-12 col-md-10 col-md-offset-1\"><div class=form-group ng-class=\"{ 'has-error' : resetForm.userPassword.$invalid &&\n" +
    "                    resetForm.userPassword.$dirty || passworderror }\"><div class=input-group><span class=input-group-addon><i class=\"glyphicon glyphicon-lock\"></i></span> <input type=password class=form-control id=user-password name=userPassword ng-model=password ng-minlength=8 required placeholder=Lösenord></div><p class=help-block ng-show=\"resetForm.userPassword.$invalid &&\n" +
    "                      resetForm.userPassword.$dirty || passworderror\"><span>Ditt lösenord måste innehålla minst 8 tecken.</span></p></div><div class=form-group ng-class=\"{ 'has-error' : resetForm.userConfirm.$invalid &&\n" +
    "                    resetForm.userConfirm.$dirty }\"><div class=input-group><span class=input-group-addon>x2</span> <input type=password class=form-control id=user-confirm name=userConfirm ng-model=confirm password-verify=password required placeholder=\"Repetera lösenord\"></div><p class=help-block ng-show=resetForm.userConfirm.$error.passwordVerify><span>Du angav inte samma lösenord som ovan.</span></p></div><div class=form-group><input type=submit class=\"btn btn-lg btn-wind btn-block\" value=\"Återställ lösenord och logga in\"></div></div></div></fieldset></form></div></div></div></div></div>");
  $templateCache.put("modules/suppliers/partials/single-supplier.tpl.html",
    "<div id=single-supplier class=container-fluid ng-init=getSingleSupplier()><div class=\"row row-offcanvas row-offcanvas-left\"><div class=\"hidden-xs col-sm-3 sidebar-offcanvas\" id=sidebarsuppliers><div class=card id=sokruta><hr><div class=\"input-group col-xs-12\"><input type=search class=form-control placeholder=\"Sök Leverantörer\" name=srch-term id=srch-term ng-model=\"search\"><div class=input-group-btn><button class=\"btn btn-default\" type=submit><i class=\"glyphicon glyphicon-search\"></i></button></div></div><hr><div ng-repeat=\"supplier in filtered = (suppliers | filter:search) | orderBy:'name'\"><a ui-sref=\"leverantorer.leverantor({ supplier_id: supplier._id })\"><h2>{{supplier.name | descriptionFilter:letterLimitHeading}}</h2></a><rating ng-model=supplier.rating max=5 data-readonly=true></rating><p>{{supplier.description | descriptionFilter:letterLimit}}</p></div></div></div><div class=\"col-xs-12 col-sm-8\"><div><alert ng-repeat=\"alert in alerts\" type={{alert.type}} close=closeAlert($index)>{{alert.msg}}</alert></div><div ng-hide=editing><div class=card><div class=row><div class=\"text-left col-xs-8\"><h1>{{selectedSupplier.name || \"Hittade ingen leverantör\"}}</h1></div><div ng-if=\"(userRole == 'creator' || userRole == 'admin' || userRole == 'editor')\" id=singleSupplierButtons class=\"text-right col-xs-4\"><div class=row><div class=\"text-right col-xs-12\"><button class=\"btn btn-info\" ng-show=selectedSupplier ng-click=setEditing(true)>Editera leverantör</button></div></div><div class=row><div class=\"text-right col-xs-12\"><button ng-show=selectedSupplier ng-click=deleteSupplier() class=\"btn btn-danger\">Ta bort leverantör</button></div></div></div><div class=\"col-xs-8 star2x\"><rating ng-model=selectedSupplier.rating max=5 data-readonly=true state-on=\"'rating-selected'\" state-off=\"'rating'\"></rating></div></div></div><div class=\"card col-xs-12\"><div class=row><div ng-show=selectedSupplier><div class=\"card col-xs-12 col-sm-6\"><h4>Segment: &nbsp; <span class=\"label label-primary\">{{selectedSupplier.segment || 'Inget segment inlagt'}}</span> <a ui-sref=\"leverantorer({filterParam: selectedSupplier.segment})\">{{selectedSupplier.segment || 'Inget segment inlagt'}}</a></h4></div><div class=\"card col-xs-12 col-sm-6\"><h4>Kategori: &nbsp; <span class=\"label label-primary\">{{selectedSupplier.category || 'Ingen kategori inlagd'}}</span></h4></div></div></div></div><div class=\"card col-xs-12 col-sm-6\"><div class=row><div ng-show=selectedSupplier><div><p>Leverantörsansvarig: {{selectedSupplier.supplierManager}}</p><hr><p>Beskrivning: {{selectedSupplier.description}}</p></div></div></div></div></div><div class=card ng-show=editing><form class=form-horizontal name=supplierEditForm><div class=row><div id=headerSingleSupplier class=\"text-left col-xs-8\"><input class=form-control id=name ng-model=selectedSupplier.name placeholder=\"Namnet på leverantören\"></div><div id=singleSupplierButtons class=\"text-right col-xs-4\"><div class=row><div class=\"text-right col-xs-12\"><button class=\"btn btn-warning\" ng-show=selectedSupplier ng-click=reset(supplierEditForm)>Avbryt</button></div></div><div class=row><div class=\"text-right col-xs-12\"><button ng-show=selectedSupplier ng-click=deleteSupplier() class=\"btn btn-danger\">Ta bort leverantör</button></div></div></div></div><div class=row><div ng-show=selectedSupplier><div class=col-xs-12><div class=form-group><span class=col-xs-12><div class=star2x><rating ng-model=selectedSupplier.rating max=5 state-on=\"'rating-selected'\" state-off=\"'rating'\"></rating></div></span></div><div class=form-group><div class=col-xs-12><label for=supplierManager class=control-label>Leverantörsansvarig:</label><input id=supplierManager class=form-control ng-model=selectedSupplier.supplierManager placeholder=\"Namn på vår leverantörsansvarig\"></div></div><hr><div class=form-group><div class=col-xs-12><label for=description class=control-label>Beskrivning:</label><span><textarea id=description class=form-control cols=30 rows=5 ng-model=selectedSupplier.description placeholder=\"En kort beskrivning av leverantören\"></textarea></span></div></div><div class=form-group><div class=col-xs-12><label for=orgnr class=control-label>Organisationsnummer:</label><span><input id=orgnr class=form-control ng-model=selectedSupplier.orgnr placeholder=\"Leverantörens organisationsnummer\"></span></div></div><div class=form-group><label for=segment.name class=\"control-label col-sm-2\">Segment:</label><br><div class=segment><div class=btn-group><label ng-repeat=\"segment in segments\" for={{segment.name}} class=\"btn btn-grey\" style=\"margin:0 1px 1px 0\">{{segment.name}} <input type=radio ng-model=selectedSupplier.segment ng-value=segment.name id={{segment.name}} name=segment></label></div></div></div><div class=form-group><label for=category.name class=\"control-label col-sm-2\">Kategori:</label><br><div class=category><div class=btn-group><label ng-repeat=\"category in categories\" for={{category.name}} class=\"btn btn-grey\" style=\"margin:0 1px 1px 0\">{{category.name}} <input type=radio ng-model=selectedSupplier.category ng-value=category.name id={{category.name}} name=category></label></div></div></div><div class=form-group><span class=\"col-lg-10 col-lg-offset-1 text-center\"><input type=submit ng-click=\"submit($event, supplierEditForm, selectedSupplier)\" value=\"Spara leverantör\" class=\"btn btn-grey btn-lg btn-block\"></span></div></div></div></div></form></div></div></div></div>");
  $templateCache.put("modules/suppliers/partials/supplier-form.tpl.html",
    "<form name=supplierForm ng-submit=createSupplier(supplier)><section class=\"row well live-preview hidden-xs\" ng-show=supplier.name><div class=\"alert alert-success\" role=alert ng-show=success>{{submitMessage}}</div><div class=\"alert alert-danger\" role=alert ng-show=failure>{{submitMessage}}</div><aside class=col-sm-3><div class=\"star text-center\"><rating ng-model=supplier.rating max=5 data-readonly=true state-on=\"'rating-selected'\" state-off=\"'rating'\"></rating></div><ul class=list-unstyled><li><span class=\"label label-primary\">{{supplier.segment}}</span></li></ul></aside><div class=col-sm-9><h3>{{supplier.name || \"Leverantörsnamn\"}}</h3><p><em>Organisationsnummer: &nbsp;</em>{{supplier.orgnr || \"Inget organisationsnummer\"}}</p><p><em>Beskrivning: &nbsp;</em>{{supplier.description || \"Ingen leverantörsbeskrivning\"}}</p><p><em>Leverantörsansvarig: &nbsp;</em>{{supplier.supplierManager || \"Ingen leverantörsansvarig\"}}</p></div></section><div class=\"input-container col-xs-12 col-sm-10 col-md-8 col-sm-offset-1 col-md-offset-2\"><fieldset class=form-group><div class=col-xs-12><label for=name class=control-label>Leverantörsnamn:</label><span><input class=form-control id=name ng-model=supplier.name placeholder=\"Namnet på leverantören\"></span></div></fieldset><fieldset class=form-group><div class=col-xs-12><label for=orgnr class=control-label>Organisationsnummer:</label><span><input id=orgnr class=form-control ng-model=supplier.orgnr placeholder=\"Leverantörens organisationsnummer\"></span></div></fieldset><fieldset class=form-group><div class=col-xs-12><label for=description class=control-label>Beskrivning:</label><span><textarea id=description class=form-control cols=30 rows=3 ng-model=supplier.description placeholder=\"En kort beskrivning av leverantören\"></textarea></span></div></fieldset><fieldset class=form-group><div class=col-xs-12><label class=control-label for=supplierManager>Leverantörsansvarig:</label><span><input id=supplierManager class=form-control ng-model=supplier.supplierManager placeholder=\"Namn på vår leverantörsansvarig\"></span></div></fieldset><fieldset class=form-group><span class=col-xs-12><label for=rating class=control-label>Rating:</label><div class=star><rating ng-model=supplier.rating max=5 state-on=\"'rating-selected'\" state-off=\"'rating'\"></rating></div></span></fieldset><fieldset class=form-group><label for=segment.name class=\"control-label col-sm-2\">Segment:</label><br><div class=segment><div class=btn-group><label ng-repeat=\"segment in segments\" for={{segment.name}} class=\"btn btn-grey\" style=\"margin:0 1px 1px 0\">{{segment.name}} <input type=radio ng-model=supplier.segment ng-value=segment.name id={{segment.name}} name=segment></label></div></div></fieldset><fieldset class=form-group><label for=category.name class=\"control-label col-sm-2\">Kategori:</label><br><div class=category><div class=btn-group><label ng-repeat=\"category in categories\" for={{category.name}} class=\"btn btn-grey\" style=\"margin:0 1px 1px 0\">{{category.name}} <input type=radio ng-model=supplier.category ng-value=category.name id={{category.name}} name=category></label></div></div></fieldset><fieldset class=form-group><span class=\"col-lg-10 col-lg-offset-1 text-center\"><input type=submit value=\"Spara leverantör\" class=\"btn btn-grey btn-lg btn-block\"></span></fieldset></div></form>");
  $templateCache.put("modules/suppliers/partials/supplier-labels.tpl.html",
    "<ul class=\"list-unstyled fix-padding\"><button type=button class=\"btn btn-primary btn-xs\">{{segment}}</button></ul>");
  $templateCache.put("modules/suppliers/partials/suppliers.tpl.html",
    "<div class=container-fluid><div class=row><div class=\"col-xs-12 col-sm-10 col-sm-offset-1\"><div class=\"jumbotron text-center\"><div id=containerIntro><p>Ni har totalt:&nbsp;&nbsp;</p><h1>{{suppliers.length}}</h1><p>&nbsp;&nbsp;leverantörer</p><br><p><h5 ng-hide=\"suppliers.length == filtered.length\">Antal valda leverantörer: {{filtered.length}}</h5></p><br><h4>Filtrera på segment:</h4><div class=btn-group><label ng-repeat=\"segment in getSegments()\" class=\"btn btn-grey\" ng-model=\"filter['segment'][segment]\" style=\"margin:0 1px 1px 0\" btn-checkbox>{{segment || 'ingen data'}}</label></div><br><h4>Filtrera på kategori:</h4><div class=btn-group><label ng-repeat=\"category in getCategories()\" class=\"btn btn-wind\" ng-model=\"filter['category'][category]\" style=\"margin:0 1px 1px 0\" btn-checkbox>{{category || 'ingen data'}}</label></div></div></div></div></div><div id=search-row class=row><div class=col-sm-3><div class=input-group-btn><ul class=inline style=\"margin-bottom: 0px\"><li><div class=btn-group-btn ng-class=\"{open: open}\"><button class=\"btn btn-grey\">Filtrera på segment</button> <button class=\"btn btn-grey dropdown-toggle\" ng-click=\"open=!open\"><span class=caret></span></button><ul class=dropdown-menu aria-labelledby=dropdownMenu><li><a ng-click=checkAll()><i class=\"glyphicon glyphicon-ok-sign\"></i> Check All</a></li><li><a ng-click=\"selectedSegment=[];\"><i class=\"glyphicon glyphicon-remove-sign\"></i> Uncheck All</a></li><li class=divider></li><li ng-repeat=\"segment in segments\"><a ng-click=setSelectedSegment()>{{segment.name}}<span ng-class=isChecked(segment.id)></span></a></li></ul></div></li></ul></div></div><div class=\"col-xs-12 col-sm-6\"><div class=text-center ng-show=showForm><h1>Lägg till leverantör</h1></div><div class=input-group ng-hide=showForm><input type=search class=form-control placeholder=\"Sök Leverantörer\" name=srch-term id=srch-term ng-model=\"search\"><div class=input-group-btn><button class=\"btn btn-default\" type=submit><i class=\"glyphicon glyphicon-search\"></i></button></div></div></div><div class=\"col-sm-1 hidden-xs\"><button class=\"btn btn-grey\" ng-click=\"$parent.showForm = !$parent.showForm\" ng-if=\"(userRole == 'creator' || userRole == 'admin' || userRole == 'editor')\"><span class=glyphicon style=vertical-align:left ng-class=\"{'glyphicon-chevron-down': showForm,\n" +
    "        'glyphicon-chevron-right': !showForm}\"></span> {{showForm ? \" Avbryt\" : \"Lägg till leverantör\"}}</button></div><div class=\"col-xs-12 col-sm-8 col-sm-offset-2\"><div ui-view=form ng-show=showForm></div></div></div><div class=row><div class=\"supplier-container col-xs-12 col-sm-10 col-sm-offset-1\"><div class=\"supplier-card col-xs-6 col-sm-4 col-md-3 col-lg-2\" ng-repeat=\"supplier in filtered = (suppliers | filter:search | filter:segmentfilter | filter:filterByProperties) | orderBy:'name'\"><div ui-sref=\"leverantorer.leverantor({ supplier_id: supplier._id })\" class=supplier-inner><div id=supplier-title><h4>{{supplier.name | descriptionFilter:letterLimitHeading}}</h4><rating ng-model=supplier.rating max=5 data-readonly=true></rating><p>{{supplier.description | descriptionFilter:letterLimit}}</p></div><div id=supplier-category><supplier-labels category=supplier.category segment=supplier.segment></supplier-labels></div></div></div></div></div></div>");
  $templateCache.put("modules/useradministration/partials/invitation-modal.tpl.html",
    "<div class=modal tabindex=-1 role=dialog><div class=\"modal-dialog modal-md\"><div class=modal-content><div class=\"modal-header text-center\"><h3 class=\"modal-title authform-header-content\">Skicka inbjudan till ny användare</h3><span type=button class=\"glyphicon glyphicon-remove pull-right authform-header-content\" id=authform-close data-dismiss=modal ng-click=dismiss()></span></div><div class=modal-body><form ng-submit=\"inviteUser(candidate, mailtext)\" name=inviteForm novalidate><div class=\"alert alert-success\" role=alert ng-show=\"success &&\n" +
    "              message\">{{message}}</div><div class=\"alert alert-danger\" role=alert ng-show=\"!success &&\n" +
    "            message\">{{message}}</div><div class=form-group ng-class=\"{ 'has-error' : (inviteForm.candidateEmail.$invalid &&\n" +
    "                inviteForm.candidateEmail.$touched) || (inviteForm.$submitted && !success) }\"><div class=input-group><span class=input-group-addon>@</span> <input type=email class=form-control id=candidate-email name=candidateEmail ng-model=candidate.email required autofocus placeholder=namn@företag.se></div><p class=help-block ng-show=\"inviteForm.candidateEmail.$invalid &&\n" +
    "                inviteForm.candidateEmail.$touched\"><span>Du måste ange en giltig epostadress.</span></p><p class=help-block ng-show=mailerror><span>Denna mail adress är redan registrerad.</span></p></div><h4>Meddelande till användaren</h4><textarea class=form-control rows=4 ng-model=mailtext>\n" +
    "            </textarea><h4>Privilegier för användaren</h4>{{explanation}}<div class=form-group><div class=btn-group><label class=\"btn btn-primary\" ng-model=candidate.role btn-radio=\"'admin'\">Admin</label><label class=\"btn btn-primary\" ng-model=candidate.role btn-radio=\"'editor'\">Editor</label><label class=\"btn btn-primary\" ng-model=candidate.role btn-radio=\"'viewer'\">Viewer</label></div></div><div class=\"modal-footer text-center\"><button type=submit ng-disabled=inviteForm.$invalid class=\"btn btn-block btn-lg btn-wind\">Bjud in användare</button></div></form></div></div></div></div>");
  $templateCache.put("modules/useradministration/partials/invitation.tpl.html",
    "<div class=container-fluid><div class=\"row row-offcanvas row-offcanvas-right\"><div class=\"col-sm-6 col-md-4 col-md-offset-4\"><h3>Skicka inbjuding till användare</h3><form ng-submit=\"inviteUser(candidate, mailtext)\" name=inviteForm novalidate><div class=\"alert alert-success\" role=alert ng-show=\"success &&\n" +
    "          message\">{{message}}</div><div class=\"alert alert-danger\" role=alert ng-show=\"!success &&\n" +
    "          message\">{{message}}</div><div class=form-group><div class=input-group><span class=input-group-addon>@</span> <input class=form-control id=candidate-email name=candidateEmail ng-model=candidate.email placeholder=namn@företag.se required autofocus></div></div><h4>Meddelande till användaren</h4><textarea class=form-control rows=4 ng-model=mailtext>\n" +
    "        </textarea><h4>Privilegier för användaren</h4>{{explanation}}<div class=form-group><div class=btn-group><label class=\"btn btn-primary\" ng-model=candidate.role btn-radio=\"'admin'\">Admin</label><label class=\"btn btn-primary\" ng-model=candidate.role btn-radio=\"'editor'\">Editor</label><label class=\"btn btn-primary\" ng-model=candidate.role btn-radio=\"'viewer'\">Viewer</label></div></div><div class=form-group><input ng-disabled=inviteForm.$invalid type=submit class=\"btn btn-lg btn-wind btn-block\" value=Skicka></div></form></div></div></div>");
  $templateCache.put("modules/useradministration/partials/removeuser-modal.tpl.html",
    "<div class=modal tabindex=-1 role=dialog><div class=\"modal-dialog modal-sm\"><div class=modal-content><div class=\"modal-header text-center\"><h4 class=\"modal-title authform-header-content\">OBS!</h4><span type=button class=\"glyphicon glyphicon-remove pull-right authform-header-content\" id=authform-close data-dismiss=modal ng-click=dismiss()></span></div><div class=\"modal-body text-center\"><p>Vill du verkligen ta bort {{user.name}}?</p></div><div class=\"modal-footer text-center\"><button class=\"btn btn-block btn-wind\" type=button ng-click=ok()>OK</button> <button class=\"btn btn-block btn-warning\" type=button ng-click=cancel()>Avbryt</button></div></div></div></div>");
  $templateCache.put("modules/useradministration/partials/single-user.tpl.html",
    "<div id=single-supplier class=container-fluid><div class=\"row row-offcanvas row-offcanvas-left\"><div class=\"col-xs-12 col-sm-8 col-sm-offset-2\"><div class=card><div class=row><div class=\"text-left col-xs-9\"><h1>{{selectedUser.name || \"Hittade ingen användare\"}}</h1></div><div class=\"text-right col-xs-3\"><button ng-if=\"(userRole == 'creator' || userRole == 'admin')\" type=button ng-show=selectedUser ng-click=deleteUser(selectedUser.user_id) class=\"btn btn-danger text-right\">Ta bort användare</button></div></div><div class=row><div ng-show=selectedUser><div class=col-xs-12><p>E-mail: {{selectedUser.email}}</p><hr><p>Rättigheter: {{selectedUser.role}}</p></div></div></div></div></div></div></div>");
  $templateCache.put("modules/useradministration/partials/user-registration.tpl.html",
    "<div class=container-fluid><div class=\"row row-offcanvas row-offcanvas-right\"><div class=\"col-sm-6 col-md-4 col-md-offset-4\"><h3>Registrera användare</h3><form ng-submit=registerUser(user) name=regForm novalidate><div class=\"alert alert-success\" role=alert ng-show=\"success &&\n" +
    "          message\">{{message}}</div><div class=\"alert alert-danger\" role=alert ng-show=\"!success &&\n" +
    "          message\">{{message}}</div><div class=form-group ng-class=\"{ 'has-error' :\n" +
    "          regForm.userName.$invalid && regForm.userName.$dirty }\"><div class=input-group><span class=input-group-addon><i class=\"glyphicon glyphicon-user\"></i></span> <input class=form-control id=user-name name=userName ng-model=user.name required autofocus placeholder=\"Ditt namn\"></div><p class=help-block ng-show=\"regForm.userName.$invalid && regForm.userName.$dirty\"><span>Du glömde ange ditt namn.</span></p></div><div class=form-group ng-class=\"{ 'has-error' : regForm.userPassword.$invalid &&\n" +
    "          regForm.userPassword.$dirty || passworderror }\"><div class=input-group><span class=input-group-addon><i class=\"glyphicon glyphicon-lock\"></i></span> <input type=password class=form-control id=user-password name=userPassword ng-model=user.password ng-minlength=8 required placeholder=Lösenord></div><p class=help-block ng-show=\"regForm.userPassword.$invalid &&\n" +
    "            regForm.userPassword.$dirty || passworderror\"><span>Ditt lösenord måste innehålla minst 8 tecken.</span></p></div><div class=form-group ng-class=\"{ 'has-error' : regForm.userConfirm.$invalid &&\n" +
    "          regForm.userConfirm.$dirty }\"><div class=input-group><span class=input-group-addon>x2</span> <input type=password class=form-control id=user-confirm name=userConfirm ng-model=user.confirm password-verify=user.password required placeholder=\"Repetera lösenord\"></div><p class=help-block ng-show=regForm.userConfirm.$error.passwordVerify><span>Du angav inte samma lösenord som ovan.</span></p></div><div class=form-group><input ng-disabled=regForm.$invalid type=submit class=\"btn btn-lg btn-wind btn-block\" value=Skicka></div></form></div></div></div>");
  $templateCache.put("modules/useradministration/partials/users.tpl.html",
    "<div class=container-fluid><div class=row><div class=\"admincards col-xs-12 col-sm-10 col-sm-offset-1\"><div class=\"col-xs-12 col-md-6 col-lg-4\"><div class=\"card darkwind text-center\"><h3><span class=\"glyphicon glyphicon-user\" aria-hidden=true></span> <small>&nbsp;&nbsp;&nbsp;Antal användare:</small> {{users.length}}</h3></div></div><div class=\"col-xs-12 col-md-6 col-lg-4\"><div class=\"card oviu-lightgrey text-center\"><h3><span class=\"glyphicon glyphicon-wrench\" aria-hidden=true></span> <small>&nbsp;Antal administratörer:</small> {{(users | filter:{role:'creator'}).length + (users | filter:{role:'admin'}).length}}</h3></div></div><div class=\"col-xs-12 col-md-6 col-lg-4\"><div class=\"card wind text-center\"><h3><span class=\"glyphicon glyphicon-list-alt\" aria-hidden=true></span> <small>&nbsp;&nbsp;&nbsp;Antal kategorier:</small> {{users.length}}</h3></div></div></div></div><div class=row><div class=\"col-sm-10 col-sm-offset-1\"><div class=col-sm-12><div class=card><div><alert ng-repeat=\"alert in alerts\" type={{alert.type}} close=closeAlert($index)>{{alert.msg}}</alert></div><button ng-if=\"(userRole == 'creator' || userRole == 'admin')\" class=\"btn btn-grey pull-right\" ng-click=openInviteModal()>Bjud in användare</button><h2>Användare hos {{org}}</h2><table class=\"table table-bordered table-striped\"><thead><tr><td><a href=# ng-click=\"sortType = 'name'; sortReverse = !sortReverse\">Namn <span ng-show=\"sortType == 'name' && !sortReverse\" class=\"fa fa-caret-down\"></span> <span ng-show=\"sortType == 'name' && sortReverse\" class=\"fa fa-caret-up\"></span></a></td><td><a href=# ng-click=\"sortType = 'email'; sortReverse = !sortReverse\">E-mail <span ng-show=\"sortType == 'email' && !sortReverse\" class=\"fa fa-caret-down\"></span> <span ng-show=\"sortType == 'email' && sortReverse\" class=\"fa fa-caret-up\"></span></a></td><td><a href=# ng-click=\"sortType = 'role'; sortReverse = !sortReverse\">Rättigheter <span ng-show=\"sortType == 'role' && !sortReverse\" class=\"fa fa-caret-down\"></span> <span ng-show=\"sortType == 'role' && sortReverse\" class=\"fa fa-caret-up\"></span></a></td><td><a href=# ng-click=\"sortType = 'role'; sortReverse = !sortReverse\">Medlem sedan <span ng-show=\"sortType == 'role' && !sortReverse\" class=\"fa fa-caret-down\"></span> <span ng-show=\"sortType == 'role' && sortReverse\" class=\"fa fa-caret-up\"></span></a></td><td><a href=# ng-click=\"sortType = 'role'; sortReverse = !sortReverse\">Senaste inloggningsförsök <span ng-show=\"sortType == 'role' && !sortReverse\" class=\"fa fa-caret-down\"></span> <span ng-show=\"sortType == 'role' && sortReverse\" class=\"fa fa-caret-up\"></span></a></td><td ng-if=\"(userRole == 'creator' || userRole == 'admin')\"><a href=# ng-click=\"sortType = 'active'; sortReverse = !sortReverse\">Aktiv? <span ng-show=\"sortType == 'active' && !sortReverse\" class=\"fa fa-caret-down\"></span> <span ng-show=\"sortType == 'active' && sortReverse\" class=\"fa fa-caret-up\"></span></a></td><td ng-if=\"(userRole == 'creator' || userRole == 'admin')\">Ta bort</td></tr></thead><tbody><tr ng-repeat=\"user in users | orderBy:sortType:sortReverse\"><td><a ui-sref=\"anvandare.enanvandare({ user_id: user._id })\">{{ user.name }}</a></td><td>{{ user.email }}</td><td>{{ user.role }}</td><td>{{ user.created_at | date : 'yyyy/MM/dd' : '+0100' }}</td><td>{{ user.lastLoginAttempt | date : 'd/M/yy' : '+0100' }}</td><td ng-if=\"(userRole == 'creator' || userRole == 'admin')\">{{ user.active ? 'Ja' : 'Nej' }}</td><td ng-if=\"(userRole == 'creator' || userRole == 'admin')\"><button ng-show=\"user.role != 'creator' || userId != user._id\" ng-click=openRemoveModal(user) type=button class=\"btn btn-default\"><span class=\"glyphicon glyphicon-trash\" aria-hidden=true></span></button></td></tr></tbody></table></div></div></div></div></div>");
}]);

}());
