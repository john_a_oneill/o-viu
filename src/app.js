/**
 * This file holds all front-end routing logic and is the entry point
 */
angular.module('supplierApp', [
  // External resources
  'ngResource',
  'ui.router',
  'ngAnimate',
  'ui.bootstrap',
  'snap',
  'angularFileUpload',
  // Internal resources
  'templates-main',
  'common',
  'modules.home',
  'modules.login',
  'modules.navbar',
  'modules.orgconfirmation',
  'modules.suppliers',
  'modules.resetpassword',
  'modules.useradministration'
])

// TODO: Use Modernizr to see if browser has: Localstorage, and other stuff that
// application requires, if not display message to upgrade browser to
// appropriate version.

// This controller mainly controlls authentication of the app and what resources
// the user is allowed to see if logged in or not.
.controller('mainCtrl', ['$rootScope', '$log', '$state', 'userService',
  function($rootScope, $log, $state, userService) {
    //Configure the snap menu
    $rootScope.snapOpts = {
      disable: 'right',
      maxPosition: 200
    };

    //For all state changes check if user is logged in
    $rootScope.$on('$stateChangeStart',
      function(event, toState, toParams, fromState, fromParams) {
        //Clear state and title for breadcrumb in header
        $rootScope.firstStateTitle = null;
        $rootScope.firstState = null;
        $rootScope.secondStateTitle = null;
        $rootScope.secondState = null;
        $rootScope.clickHandler = null;

        //Store status true or false of user is logged in in rootScope
        var loggedIn = userService.isLoggedIn();
        $rootScope.isLoggedIn = loggedIn;

        //Get the current user role and store it in rootScope
        if(loggedIn) {
          userService.getCurrentUser(
            function (err) {
            }, function (resp) {
              var currentUser = resp.data;
              if(!currentUser) { // no data retreived
              } else {
              $rootScope.userRole = currentUser.role; //Store user role in rootScope
              $rootScope.userName = currentUser.name; //Store user name in rootScope
              $rootScope.userId = currentUser.userid; //Store user ID in rootScope
              }
            }
          );
        }
        
        if ((toState.access.type === "private") && !loggedIn) {
          event.preventDefault(); //prevents from resolving requested url
          $state.go('login');
        }
      }
    );

    // logout userfunction on every child scope
    $rootScope.logoutUser = function() {
      userService.logout();
    };
  }
])

// Configuration of application
.config(['$locationProvider', '$httpProvider', '$urlRouterProvider',
  function($locationProvider, $httpProvider, $urlRouterProvider) {
    // enable html5Mode for pushstate ('#'-less URLs)
    $locationProvider.html5Mode(true);

    // attach our auth interceptor to all the http requests
    $httpProvider.interceptors.push('authInterceptorService');

    // redirect users to 404 if not found route
    $urlRouterProvider.when('/404',
      ['$state', 'userService',
        function ($state, userService) {
          if(userService.isLoggedIn) $state.go('hem');
          else $state.go('login');
        }
      ]
    );
    $urlRouterProvider.otherwise('/404');
  }
]);
