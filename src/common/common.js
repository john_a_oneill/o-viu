angular.module('common', [
  // Services
  'common.services.userservice',
  'common.services.orgservice',
  'common.services.supplierservice',
  'common.services.authservice',
  'common.services.authinterceptorservice',
  // Directives
  'common.directives.passwordverify',
  // Filter
  'common.filters.descriptionfilter',
  'common.filters.segmentFilter',
  'common.filters.capitalizeFirst'
]);
