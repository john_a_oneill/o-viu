angular.module('common.filters.capitalizeFirst', [])

 .filter('capitalizeFirst', [function () {
    return function (str) {
        str = str || '';
        return str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase();
    };
}]);