angular.module('common.filters.segmentFilter', [])

 .filter('segmentFilter', [function () {
    return function( items, types) {
	    var filtered = [];
	    
	    angular.forEach(items, function(item) {
	       if(types.luxury == false && types.double_suite == false) {
	          filtered.push(item);
	        }
	        else if(types.luxury == true && types.double_suite == false && item.type == 'luxury'){
	          filtered.push(item);
	        }
	        else if(types.double_suite == true && types.luxury == false && item.type == 'double suite'){
	          filtered.push(item);
	        }
	    });
	  
	    return filtered;
	};
}]);