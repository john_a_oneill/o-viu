// This service is for handling user requests (returns promises)
angular.module('common.services.authservice', [])

.factory('authService', ['$window', function($window) {
  var authService = {};

  authService.getToken = function() {
    return $window.localStorage.getItem('o-viu-signature');
  };

  authService.setToken = function(token) {
    $window.localStorage['o-viu-signature'] = token;
  };

  authService.clearToken = function() {
    $window.localStorage.removeItem('o-viu-signature');
  };

  return authService;

}]);
