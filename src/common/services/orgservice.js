// This service is for handling user requests (returns promises)
angular.module('common.services.orgservice', [])

.factory('orgService', ['$http', '$log', '$q', '$timeout', '$window', '$rootScope',
  function($http, $log, $q, $timeout, $window, $rootScope) {
    var orgService = {};

    orgService.create = function (userdata, errorhandler, callback) {
      $http.post('/api/register', userdata)
      .then(callback)
      .catch(errorhandler);
    };

    orgService.confirm = function(id, hash, errorhandler, callback) {
      $http.get('/api/confirm/' + id + '/' + hash)
      .then(callback)
      .catch(errorhandler);
    };

    orgService.getCurrentOrg = function(errorhandler, callback) {
      $http.get('/api/myorg')
      .then(callback)
      .catch(errorhandler);
    };

    return orgService;
  }
]);
