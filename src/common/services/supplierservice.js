angular.module('common.services.supplierservice', [])

// TODO: How are these factories later used? Should there be a array in
// suppliers schema or on organisation?
.factory('segmentFactory', [function () {
  var segments = 
  [{
    id: 1,
    name: 'partner'
  }, {
    id: 2,
    name: 'föredragna'
  }, {
    id: 3,
    name: 'övriga'
  }];
  return segments;
}])

.factory('categoryFactory', [function () {
  var categories = 
  [{
    id: 1,
    name: 'produktion'
  }, {
    id: 2,
    name: 'råmaterial'
  }, {
    id: 3,
    name: 'logistik'
  }, {
    id: 4,
    name: 'entreprenad'
  }, {
    id: 5,
    name: 'fastigheter'
  }, {
    id: 6,
    name: 'kontorsmaterial'
  }, {
    id: 7,
    name: 'IT'
  }];
  return categories;
}])

// TODO: Hook up this to supplier handling
.factory('supplierFactory',

//  ['$http', '$rootScope', '$resource',
//    function ($http, $rootScope, $resource) {

  ['$http', '$q', '$log', '$resource', '$location', 'authService', 'orgService',
    function ($http, $q, $log, $resource, $location, authService, orgService) {
      var supplierService = {};

      supplierService.suppliers = [];

      // Get all suppliers
      supplierService.get = function () {
        // var orgid = $rootScope.org._id;
        // return $http.get('/api/supplier/' + orgid);
        orgService.getCurrentOrg(
          function (err) { /* TODO: do something and do same thing below */ },
          function (resp1) {
            if (resp1.data.success) {
              $http.get('/api/supplier/' + resp1.data.data._id)
              .then(function (resp2) {
                if (resp2.data.success)
                  supplierService.suppliers = resp2.data.data;
              });
              // TODO: do it here
            }
          }
        );
      };

      //Create new supplier
      supplierService.create = function (supplierdata, errorhandler, callback) {
        orgService.getCurrentOrg(
        errorhandler,
        function (resp) {
          $http.post('/api/supplier/' + resp.data.data._id, supplierdata)
          .then(callback)
          .catch(errorhandler)
        });
      };

      //Get one supplier from params
      supplierService.getOne = function (supplier_id, errorhandler, callback) {
        orgService.getCurrentOrg(
        errorhandler,
        function (resp) {
          $http.get('/api/supplier/' + resp.data.data._id + '/' + supplier_id)
          .then(callback)
          .catch(errorhandler);
        });
      };

      //Get one supplier from params
      supplierService.update = function (supplier_id, supplierdata, errorhandler, callback) {
        orgService.getCurrentOrg(
        errorhandler,
        function (resp) {
          $http.put('/api/supplier/' + resp.data.data._id + '/' + supplier_id, supplierdata)
          .then(callback)
          .catch(errorhandler);
        });
      };

      //Delete supplier
      supplierService.delete = function (supplier_id, errorhandler, callback) {
        orgService.getCurrentOrg(
        errorhandler,
        function (resp) {
          $http.delete('/api/supplier/' + resp.data.data._id + '/' + supplier_id)
          .then(callback)
          .catch(errorhandler);
        });
      };

      return supplierService;
    }
  ]
);
