// This service is for handling user requests (returns promises)
angular.module('common.services.userservice', [])

.factory('userService',
  ['$http', '$q', '$log', '$rootScope', '$state', '$window', 'authService', 'orgService',
    function ($http, $q, $log, $rootScope, $state, $window, authService, orgService) {
      var userService = {};

      // lists all users
      userService.all = function (orgName, errorhandler, callback) {
        $http.get('/api/user/' + orgName)
        .then(callback)
        .catch(errorhandler);
      };

      //Get one user from params
      userService.getOne = function (user_id, errorhandler, callback) {
        orgService.getCurrentOrg(
        errorhandler,
        function (resp) {
          $http.get('/api/user/' + resp.data.data._id + '/' + user_id)
          .then(callback)
          .catch(errorhandler);
        });
      };

      //Delete one user from params
      userService.delete = function (user_id, errorhandler, callback) {
        orgService.getCurrentOrg(
        errorhandler,
        function (resp) {
          $http.delete('/api/user/' + resp.data.data._id + '/' + user_id)
          .then(callback)
          .catch(errorhandler);
        });
      };

      // admin invites user
      userService.invite = function (userdata, errorhandler, callback) {
        $http.post('/api/user', userdata)
        .then(callback)
        .catch(errorhandler);
      };

      // user is confirming after having clicked on confirmlink
      userService.confirm = function (userdata, errorhandler, callback) {
        $http.post('/api/confirm', userdata)
        .then(callback)
        .catch(errorhandler);
      };

      // called when user confirm he/she has forgot password
      userService.recoverPassword = function (userdata, errorhandler, callback) {
        $http.post('/api/recoverpassword', userdata)
        .then(callback)
        .catch(errorhandler);
      };

      // called when user has pressed link in mail confirming he/she is the
      // correct user
      userService.resetPasswordAndLogin = function (userdata, errorhandler, callback) {
        $http.post('/api/resetandlogin', userdata)
        .then(callback)
        .catch(errorhandler);
      };

      userService.login = function (userdata, errorhandler, callback) {
        $http.post('/api/login', userdata)
        .then(callback)
        .catch(errorhandler);
      };

      userService.isLoggedIn = function() {
        if (authService.getToken())
          return true;
        else
          return false;
      };

      userService.getCurrentUser = function(errorhandler, resp) {
        $http.get('/api/me')
        .then(resp)
        .catch(errorhandler);
      };

      // clears out any sensitive information
      userService.logout = function() {
        authService.clearToken();
        $state.go('login');
      };

      return userService;
    }
  ]
);


