angular.module('home.controllers.homectrl', [])

.controller('HomeCtrl', ['$scope', '$timeout', '$log', 'userService', 'categoryFactory', 'orgService', 'supplierFactory',
  function ($scope, $timeout, $log, userService, categoryFactory, orgService, supplierFactory) {
    
    //Get categories from factory
    $scope.categories = categoryFactory;

    //First get the organisation and store it in the scope and then get the users from that organisation.
    orgService.getCurrentOrg(
      function (err) { /* TODO: handle error somehow */ },
      function (resp) {
        if(resp.data.data.name) {
          $scope.org = resp.data.data.name;
          userService.all($scope.org,
            function (err) { /* TODO: should handle error */ },
            function (resp) {
              var data = resp.data.data;
              $scope.users = data;
            }
          );
        }
      }
    );

    userService.getCurrentUser(
      function (err) { /* TODO: handle error somehow */ },
      function (resp) {
        if(resp.data)
          $scope.user = resp.data;
      }
    );

    supplierFactory.get(); // get all suppliers
      
    $scope.$watch( // watch for new suppliers in supplierFactory
      function() {
        return supplierFactory.suppliers;
      },
      function (newval, oldval) {
        if(newval.length !== oldval.length) {
          $timeout(function() {
            $scope.suppliers = newval;
          }, false); // to avoid flickering, asynchronous trick :-)
        } else {
            $scope.suppliers = oldval;
        }
      }
    );
  }
]);
