angular.module('modules.home', ['home.controllers.homectrl'])

.config(['$stateProvider', function($stateProvider) {

  $stateProvider.state('hem', {
    url: '/hem',
    views: {
      'main@': {
        controller: 'HomeCtrl',
        templateUrl: 'modules/home/partials/home.tpl.html'
      }
    },
    access: {
      type: "private",
      groups: "all"
    }
  });
}]);
