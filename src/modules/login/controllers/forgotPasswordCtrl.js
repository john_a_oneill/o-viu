angular.module('login.controllers.forgotpasswordctrl', [])

/*
 * This controller controls the forgotpassword modal
 */
.controller('ForgotPasswordCtrl',
  ['$scope', '$modalInstance', '$timeout', 'userService',
    function($scope, $modalInstance, $timeout, userService) {

      // To be filled in by the user through the form
      $scope.userdata = {};

      // User posts registration form
      $scope.createRecoveryLink = function(userdata) {
        userService.recoverPassword(userdata,
          // Something went wrong with connection
          function(err) {
            $scope.success = false;
            $scope.message = "Något gick fel. Försök igen och kontakta support om felet kvarstår.";
          },
          // success or user-mail is not registered
          function(resp) {
            var data = resp.data;
            $scope.success = data.success; // false or true
            $scope.message = data.message; // alert message
            if(data.success) { // close modal in 2 sec if successfull registration
              $timeout(function() {
                $modalInstance.close('success');
              }, 2000);
            }
          }
        );
      };

      // Closes modal if user cancels registration
      $scope.forgotPasswordDismiss = function() {
        $modalInstance.dismiss('cancel');
      };

    }
  ]
);
