angular.module('login.controllers.loginctrl', [])

/*
 * This controller controls the loginform and the registration&password modal
 */
.controller('LoginCtrl',
  ['$rootScope', '$scope', '$log', '$state', '$modal', '$timeout', 'orgService', 'userService', 'authService',
    function ($rootScope, $scope, $log, $state, $modal, $timeout, orgService, userService, authService) {

      // User posts login form
      $scope.loginUser = function (user) {
        userService.login(user,
          function (err) {
            $scope.failure = true;
            $scope.message = "Något gick fel. Försök igen och kontakta support om felet kvarstår.";
          },
          function (resp) {
            var data = resp.data;
            if(!data.success) { // failure: (wrong password or email)
              $scope.failure = true;
              $scope.message = data.message; // pass message from server on to user
              $scope.user.password = '';
            } else { // success: store token, user and org
              var data = data.data;
              authService.setToken(data.token);
              
              //Get current user data to store role in rootScope
              userService.getCurrentUser(
                function (err) {
                  $scope.failure = true;
                  $scope.message = "Kunde inte hämta din användare";
                }, function (resp) {
                  var currentUser = resp.data;
                  if(!currentUser) { // no data retreived
                    $scope.failure = true;
                    $scope.message = data.message; // pass message from server on to user
                  } else {
                  $rootScope.userRole = currentUser.role; //Store user role in rootScope
                  $rootScope.userName = currentUser.name; //Store user name in rootScope
                  $rootScope.userId = currentUser.userid; //Store user ID in rootScope
                  }
                }
              );

              $state.go("hem");

            }
          }
        );
      };

      // Function for loading modal (registration)
      $scope.showRegistration = function () {
        // Modal for registration
        var regModal = $modal.open({
          templateUrl: 'modules/login/partials/registration-modal.tpl.html',
          controller: 'RegistrationCtrl'
        });
        // function for when result gets back from registration
        regModal.result.then(function (data) {
          $state.go("login");
        });
      };

      // Function for loading modal (registration)
      $scope.forgotPassword = function () {
        // Modal for registration
        var forgotPasswordModal = $modal.open({
          templateUrl: 'modules/login/partials/forgotpassword-modal.tpl.html',
          controller: 'ForgotPasswordCtrl'
        });
        // function for when result gets back from registration
        forgotPasswordModal.result.then(function (data) {
          $state.go("login");
        });
      };

    }
  ]
);
