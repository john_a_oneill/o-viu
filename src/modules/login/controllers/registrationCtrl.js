angular.module('login.controllers.registrationctrl', [])

/*
 * This controller controls the registrationform modal
 */
.controller('RegistrationCtrl',
  ['$scope', '$log', '$modalInstance', '$timeout', 'orgService',
    function ($scope, $log, $modalInstance, $timeout, orgService) {

      // To be filled in by the user through the form
      $scope.user = {};

      // Clear errors, reinsert data from user and mark appropriate errors
      $scope.reset = function() {
        $scope.regForm.$setPristine();
        $scope.user = $scope.copy; // put data back in form
        $scope.user.password = '';
        $scope.user.confirm = '';
      };

      // User posts registration form
      $scope.createOrg = function(user) {
        $scope.copy = angular.copy(user); // put data back in form later
        orgService.create(user,
          // Something went wrong with connection
          function (err) {
            $scope.success = false;
            $scope.message = "Något gick fel. Försök igen och kontakta support om felet kvarstår.";
            $scope.reset();
          },
          // success or user-mail is already registered, or incorrect password
          function (resp) {
            var data = resp.data;
            $scope.success = data.success; // false or true
            $scope.message = data.message; // alert message
            if(data.success) { // close modal in 2 sec if successfull registration
              $timeout(function() {
                $modalInstance.close('success');
              }, 2000);
            } else { // not successfull (org or mail was registered or password incorrect)
              $log.log(resp);
              $scope.reset();
            }
          }
        );
      };

      // Closes modal if user cancels registration
      $scope.regDismiss = function() {
        $modalInstance.dismiss('cancel');
      };

    }
  ]
);
