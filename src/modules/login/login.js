angular.module('modules.login', [
        'common',
	      'login.controllers.loginctrl',
	      'login.controllers.registrationctrl',
        'login.controllers.forgotpasswordctrl'
	])

.config(['$stateProvider', function($stateProvider) {
  $stateProvider.state('login', {
    url: '/login',
    views: {
      'main@': {
        controller: 'LoginCtrl',
        templateUrl: 'modules/login/partials/login.tpl.html'
      }
    },
    access: {
      type: "public",
      groups: "all"
    }
  });
}]);
