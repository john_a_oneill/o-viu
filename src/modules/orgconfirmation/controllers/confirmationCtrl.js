angular.module('orgconfirmation.controllers.confirmationctrl', [])

.controller('ConfirmationCtrl',
  ['$scope', '$state', '$stateParams', '$timeout', 'orgService',
    function($scope, $state, $stateParams, $timeout, orgService) {
      var id   = $stateParams.id,
          hash = $stateParams.hash;

      orgService.confirm(id, hash,
        function(err) {
          $scope.success = false;
          $scope.message = "Något gick fel, vi kunde inte bekräfta din användare. Kontakta support.";
          $timeout(function() {
            $state.go('login');
          }, 3000);
        },
        function(resp) {
          var data = resp.data;
          $scope.success = data.success;
          $scope.message = data.message;
          $timeout(function() {
            $state.go('login');
          }, 3000);
        }
      );

    }
  ]
);
