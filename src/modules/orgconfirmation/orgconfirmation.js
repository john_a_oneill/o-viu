angular.module('modules.orgconfirmation', [
    'orgconfirmation.controllers.confirmationctrl'
    ])

.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

  $stateProvider.state('confirm', {
    url: '/confirm/:id/:hash',
    views: {
      'main@': {
        controller: 'ConfirmationCtrl',
        templateUrl: 'modules/orgconfirmation/partials/confirmation.tpl.html'
      }
    },
    access: {
      type: "public",
      groups: "all"
    }
  });

}]);
