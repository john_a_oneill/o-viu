angular.module('resetpassword.controllers.resetpasswordctrl', [])

.controller('ResetPasswordCtrl',
  ['$scope', '$state', '$rootScope', '$log', '$stateParams', 'userService', 'authService',
    function ($scope, $state, $rootScope, $log, $stateParams, userService, authService) {

      $scope.resetPasswordAndLogin = function(password) {
        var userdata = {
          password: password,
          orgid: $stateParams.orgid,
          userid: $stateParams.userid,
          hash: $stateParams.hash
        };
        userService.resetPasswordAndLogin(userdata,
          function (err) {
            $scope.failure = true;
            $scope.message = "Något gick fel. Försök igen och kontakta support om felet kvarstår.";
          },
          function (resp) {
            var data = resp.data;
            if(!data.success) { // failure: (wrong password or email)
              $scope.failure = true;
              $scope.message = data.message; // pass message from server on to user
              $scope.password = '';
              $scope.confirm = '';
            } else { // success (login): store token, user and org
              authService.setToken(data.data.token);
              $state.go("hem");
            }
          }
        );
      }

    }
  ]
);
