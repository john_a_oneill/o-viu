angular.module('modules.resetpassword', [
  'common',
  'resetpassword.controllers.resetpasswordctrl'
])

.config(['$stateProvider', function($stateProvider) {

  $stateProvider.state('reset-password', {
    url: '/reset-password/:orgid/:userid/:hash',
    views: {
      'main@': {
        controller: 'ResetPasswordCtrl',
        templateUrl: 'modules/resetpassword/partials/reset-password.tpl.html'
      }
    },
    access: {
      type: "public",
      groups: "all"
    }
  });

}]);
