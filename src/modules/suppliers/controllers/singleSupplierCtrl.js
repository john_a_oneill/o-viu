angular.module('suppliers.controllers.singlesupplierctrl', [])

.controller('SingleSupplierCtrl', ['$scope', '$rootScope', '$timeout', '$log', '$state', 'supplierFactory', '$stateParams', 'categoryFactory', 'segmentFactory',
  function ($scope, $rootScope, $timeout, $log, $state, supplierFactory, $stateParams, categoryFactory, segmentFactory) {

    $scope.letterLimit = 120;
    $scope.letterLimitHeading = 30;
    $scope.suppliers = [];
    $scope.selectedSupplier = [];
    $scope.categories = categoryFactory;
    $scope.segments = segmentFactory;
    $scope.editing = false;
    $scope.alerts = [];

    var setBreadcrumbs = function () {
      $rootScope.firstStateTitle = 'Leverantörer';
      $rootScope.secondStateTitle = $scope.selectedSupplier.name;
    };

    //Clickhandler for breadcrumb state change
    $rootScope.clickHandler = function (param){
      $state.go('leverantorer');
    };

    $scope.setEditing = function (state) {
      $scope.editing = state;
    };

    //Closes the alert bow
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };

    // get all suppliers
    supplierFactory.get(); 

    // watch for new suppliers in supplierFactory
    $scope.$watch( 
      function() {
        return supplierFactory.suppliers;
      },
      function (newval, oldval) {
        if(newval.length !== oldval.length) {
          $timeout(function() {
            $scope.suppliers = newval;
          }, false); // to avoid flickering, asynchronous trick :-)
        } else {
            $scope.suppliers = oldval;
        }
      }
    );

    // Get one supplier from state params and ID (immediately executed)
    $scope.getSingleSupplier = function () {
      supplierFactory.getOne($stateParams.supplier_id,
        function (err) { /* TODO: should handle error somehow */ },
        function (resp) {
          if(resp.data.success) {
            $scope.selectedSupplier = resp.data.data;
            setBreadcrumbs();
          }
        }
      );
    };
    

    //Called when aborting the editing form
    $scope.reset = function (form) {
      $scope.editing = false;
      $scope.getSingleSupplier(); //Get the single supplier again
      if (form) {
        form.$setPristine();
        form.$setUntouched();
      }
    };

    //Called from ngClick when submitting the editing form
    $scope.submit = function($event, form, supplier) {
      //Calls factory to update supplier from form data
      supplierFactory.update($stateParams.supplier_id, supplier,
        function (err) { // errorhandler
          //Push a new alert
          $scope.alerts.push({
            msg: 'Kunde inte uppdatera' + supplier.name, 
            type: 'danger'
          });
        },
        function (resp) { // onsuccess
          var data = resp;
          if (data.data) {
            //Push a new alert
            var alert = {};
            alert.msg = data.data.message;
            if (data.data.success) {
              alert.type = 'success';
            } else {
              alert.type = 'danger';
            }
            $scope.alerts.push({
              msg: alert.msg, 
              type: alert.type
            });
          //$scope.supplierCopy = data.config.data;
          $scope.editing = false;
          form.$setPristine();
          form.$setUntouched();
          }
        }
      );
    };

    $scope.deleteSupplier = function () {
      supplierFactory.delete($stateParams.supplier_id,
        function (err) { /* TODO: should handle error somehow */ },
        function (resp) {
          $state.go("leverantorer");
        }
      );
    }


  // $scope.itemsPerPage = 4;
  // $scope.currentPage = 1;
  // $scope.pageCount = function () {
  //   console.log(Math.ceil($scope.suppliers.length / $scope.itemsPerPage));
  //   return Math.ceil($scope.suppliers.length / $scope.itemsPerPage);
  // };
  // $scope.suppliers.$promise.then(function () {
  //   $scope.totalItems = $scope.suppliers.length;
  //   $scope.$watch('currentPage + itemsPerPage', function() {
  //     var begin = (($scope.currentPage - 1) * $scope.itemsPerPage),
  //       end = begin + $scope.itemsPerPage;
  //     $scope.filteredFriends = $scope.friends.slice(begin, end);
  //     });
  //   });
  }
]);
