angular.module('suppliers.controllers.suppliersctrl', [])

.controller('SuppliersCtrl',
  ['$scope', '$rootScope', '$timeout', '$log', '$stateParams', 'supplierFactory', 'categoryFactory', 'segmentFactory',
    function ($scope, $rootScope, $timeout, $log, $stateParams, supplierFactory, categoryFactory, segmentFactory) {

      $scope.categories = categoryFactory;
      $scope.segments = segmentFactory;
      $scope.suppliers = [];

      //Set filter from URL params
      if ($stateParams.filterParam) {
        console.log($stateParams.filterParam);
        $scope.filter = $stateParams.filterParam;
      } else {
        //Filter variable to hold bolean values for properties
        $scope.filter = { };
      }

      //Set breadcrumb
      $rootScope.firstStateTitle = 'Leverantörer';

      

      //scope variable for drop-down filter
      $scope.selectedSegment = [];

      $scope.letterLimit = 90;
      $scope.letterLimitHeading = 30;
      $scope.showForm = false;

      supplierFactory.get(); // get all suppliers
      
      $scope.$watch( // watch for new suppliers in supplierFactory
        function() {
          return supplierFactory.suppliers;
        },
        function (newval, oldval) {
          if(newval.length !== oldval.length) {
            $timeout(function() {
              $scope.suppliers = newval;
            }, false); // to avoid flickering, asynchronous trick :-)
          } else {
              $scope.suppliers = oldval;
          }
        }
      );

      // //List filter segment
      // $scope.setSelectedSegment = function () {
      //   var id = this.segment.id;
      //   if (_.contains($scope.selectedSegment, id)) {
      //     $scope.selectedSegment = _.without($scope.selectedSegment, id);
      //   } else {
      //     $scope.selectedSegment.push(id);
      //   }
      //   return false;
      // };

      // $scope.isChecked = function (id) {
      //   if (_.contains($scope.selectedSegment, id)) {
      //     return 'glyphicon glyphicon-ok pull-right';
      //   }
      //   return false;
      // };

      // $scope.checkAll = function () {
      //   $scope.selectedSegment = _.pluck($scope.segments, 'id');
      // };

      //Get the currently used segments from suppliers scope
      $scope.getSegments = function () {
        return ($scope.suppliers || []).map(function (supplier) {
          return supplier.segment;
        }).filter(function (supplier, index, arr) {
          return arr.indexOf(supplier) === index;
        });
      };

      //Get the currently used categories from suppliers scope
      $scope.getCategories = function () {
        return ($scope.suppliers || []).map(function (supplier) {
          return supplier.category;
        }).filter(function (supplier, index, arr) {
          return arr.indexOf(supplier) === index;
        });
      };


      //Filter to handle multiple properties
      $scope.filterByProperties = function (supplier) {
        // Use this snippet for matching with AND
        var matchesAND = true;
        for (var prop in $scope.filter) {
          if (noSubFilter($scope.filter[prop])) continue;
          if (!$scope.filter[prop][supplier[prop]]) {
            matchesAND = false;
            break;
          }
        }
        return matchesAND;
/**/
/*
        // Use this snippet for matching with OR
        var matchesOR = true;
        for (var prop in $scope.filter) {
            if (noSubFilter($scope.filter[prop])) continue;
            if (!$scope.filter[prop][wine[prop]]) {
                matchesOR = false;
            } else {
                matchesOR = true;
                break;
            }
        }
        return matchesOR;
/**/
      };
      
      function noSubFilter(subFilterObj) {
        for (var key in subFilterObj) {
          if (subFilterObj[key]) return false;
        }
        return true;
      }
      //End controller
    }
  ]
);
