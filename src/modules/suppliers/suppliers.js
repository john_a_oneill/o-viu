angular.module('modules.suppliers', [
    'common',
    'suppliers.controllers.suppliersctrl',
    'suppliers.controllers.singlesupplierctrl',
    'suppliers.controllers.supplierformctrl'
 ])

.config(['$stateProvider', function($stateProvider) {

  $stateProvider.state('leverantorer', {
    url: '/leverantorer/:filterParam',
    views: {
      "main@": {
        controller: 'SuppliersCtrl',
        templateUrl: 'modules/suppliers/partials/suppliers.tpl.html'
      },
      "form@leverantorer": {
        controller: 'SupplierFormCtrl',
        templateUrl: 'modules/suppliers/partials/supplier-form.tpl.html',
      }
    },
    access: {
      type: "private",
      groups: "all"
    }
  })

  .state('leverantorer.leverantor', {
    url: '/:supplier_id',
    views: {
      "main@": {
        controller: 'SingleSupplierCtrl',
        templateUrl: 'modules/suppliers/partials/single-supplier.tpl.html',
      }
    },
    access: {
      type: "private",
      groups: "all"
    }
  })

  ;
}])

.directive('supplierLabels', function() {
  return {
    restrict: 'E',
    templateUrl: 'modules/suppliers/partials/supplier-labels.tpl.html',
    scope: {
      category: '=',
      segment: '='
    }
  }
});

// .directive('supplierForm', function(){
//     return {
//       restrict: 'E',
//       templateUrl: 'modules/suppliers/partials/supplier-form.tpl.html',
//       replace: true,
//       controller: 'SupplierFormCtrl as form',
//       scope: false
//       // scope: {
//       //   category: '=',
//       //   segment: '='
//       // }
//     }
//   })


// //Makes breadcrumb resizeable
// $(document).ready(function(){
//     $(window).resize(function() {
//         ellipses1 = $("#bc1 :nth-child(2)")
//         if ($("#bc1 a:hidden").length >0) {ellipses1.show()} else {ellipses1.hide()}
//     })
// });
