angular.module('useradministration.controllers.invitationmodalinstancectrl', [])

.controller('InvitationModalInstanceCtrl',
  ['$log', '$modalInstance', '$scope', '$timeout',  'userService',
    function ($log, $modalInstance, $scope, $timeout, userService) {
      
      $scope.candidate = {};
      $scope.candidate.role = 'viewer';
      $scope.mailtext = '';
      $scope.explanation = "Välj rättigheter för användaren";

      $scope.$watch('candidate.role', function (value) {
        switch(value) {
          case("admin"):
            $scope.explanation = "Admin (användarredigering, leverantörsredigering, leverantörsbevakning)";
            break;
          case("editor"):
            $scope.explanation = "Editor (leverantörsredigering, leverantörsbevakning)";
            break;
          case("viewer"):
            $scope.explanation = "Viewer (leverenatörsbevakning)";
            break;
          default:
            $scope.explanation = "Välj rättigheter för användaren.";
        }
      });

      // Clear form
      $scope.reset = function() {
        $scope.inviteForm.$setPristine();
      };

      $scope.inviteUser = function (candidate, mailtext) {
        var userdata = {
          candidate: candidate,
          mailtext: mailtext
        };
        console.log(userdata);
        userService.invite(userdata,
          function (err) {
            $scope.success = false;
            $scope.message = "Något gick fel vid inbjudan. Kontakta support.";
            $scope.reset();
          },
          function (resp) {
            var data = resp.data;
            $scope.success = data.success;
            $scope.message = data.message;
            if(data.success) { // close modal in 2 sec if successfull registration
              $timeout(function() {
                $modalInstance.close('success');
              }, 2000);
            } else { // not successfull (org or mail was registered or password incorrect)
              $scope.reset();
            }
          }
        );
      };

      // Closes modal if user cancels invitation
      $scope.dismiss = function() {
        $modalInstance.dismiss('cancel');
      };
      //End controller
    }
  ]
);
