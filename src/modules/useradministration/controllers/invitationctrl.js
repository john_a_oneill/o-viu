angular.module('useradministration.controllers.invitationctrl', [])

.controller('InvitationCtrl',
  ['$log', '$scope', '$state', '$stateParams', '$timeout', 'userService',
    function ($log, $scope, $state, $stateParams, $timeout, userService) {
      $scope.candidate = {};

      $scope.explanation = "Välj rättigheter för användaren";
      $scope.$watch('candidate.role', function (value) {
        switch(value) {
          case("admin"):
            $scope.explanation = "Admin (användarredigering, leverantörsredigering, leverantörsbevakning)";
            break;
          case("editor"):
            $scope.explanation = "Editor (leverantörsredigering, leverantörsbevakning)";
            break;
          case("viewer"):
            $scope.explanation = "Viewer (leverenatörsbevakning)";
            break;
          default:
            $scope.explanation = "Välj rättigheter för användaren.";
        }
      });

      $scope.inviteUser = function (candidate, mailtext) {
        var userdata = {
          candidate: candidate,
          mailtext: mailtext
        };
        userService.invite(userdata,
          function (err) {
            $scope.success = false;
            $scope.message = "Något gick fel vid inbjudan. Kontakta support.";
          },
          function (resp) {
            var data = resp.data;
            $scope.success = data.success;
            $scope.message = data.message;
          }
        );
      };

    }
  ]
);
