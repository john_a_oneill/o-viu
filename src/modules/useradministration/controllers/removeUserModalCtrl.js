angular.module('useradministration.controllers.removeusermodalctrl', [])

.controller('RemoveUserModalCtrl',
  ['$log', '$modalInstance', '$scope', '$timeout', 'user',
    function ($log, $modalInstance, $scope, $timeout, user) {
      
      $scope.user = user;

      $scope.ok = function () {
        $modalInstance.close(true);
      };

      // Closes modal if user cancels registration
      $scope.dismiss = function() {
        $modalInstance.dismiss('cancel');
      };
      //End controller
    }
  ]
);
