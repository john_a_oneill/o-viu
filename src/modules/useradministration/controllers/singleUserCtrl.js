angular.module('useradministration.controllers.singlesuserctrl', [])

.controller('SingleUserCtrl', ['$scope', '$rootScope', '$timeout', '$log', '$state', 'userService', '$stateParams',
  function ($scope, $rootScope, $timeout, $log, $state, userService, $stateParams) {

    $scope.letterLimit = 120;
    $scope.letterLimitHeading = 30;

    var setBreadcrumbs = function () {
      $rootScope.firstStateTitle = 'Användaradministration';
      $rootScope.secondStateTitle = $scope.selectedUser.name;
    };

    //Clickhandler for breadcrumb state change
    $rootScope.clickHandler = function (param){
      $state.go('anvandare');
    }

    // Get one user from state params and ID (immediately executed)
    userService.getOne($stateParams.user_id,
      function (err) { 
        console.log('Error in getting a user' + err);
      },
      function (resp) {
        if(resp.data)
          //Load response data into the scope
          $scope.selectedUser = resp.data.data;

          //Variables for breadcrumb in header
          setBreadcrumbs();
      }
    );

    //Delete user by user_id from params
    $scope.deleteUser = function (user_id) {
      userService.delete($stateParams.user_id,
        function (err) { /* TODO: should handle error somehow */ },
        function (resp) {
          $state.go("anvandare");
        }
      );
    }
    //End controller
  }
]);
