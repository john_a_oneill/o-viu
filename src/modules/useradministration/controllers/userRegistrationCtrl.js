angular.module('useradministration.controllers.userregistrationctrl', [])

/*
 * This controller controls the registrationform modal
 */
.controller('UserRegistrationCtrl',
  ['$scope', '$log', '$state', '$stateParams', '$timeout', 'userService',
    function($scope, $log, $state, $stateParams, $timeout, userService) {

      // To be filled in by the user through the form
      $scope.user = {};

      // Clear errors, reinsert data from user and mark appropriate errors
      $scope.reset = function() {
        $scope.regForm.$setPristine();
        $scope.user = $scope.copy; // put data back in form
        $scope.user.password = '';
        $scope.user.confirm = '';
      };

      // User posts registration form
      $scope.registerUser = function(user) {
        $scope.copy = angular.copy(user); // put data back in form later
        userService.confirm({
            userdata: user,
            orgid: $stateParams.orgid,
            candid: $stateParams.candid,
            hash: $stateParams.hash
          },
          // Something went wrong with connection
          function (err) {
            $scope.success = false;
            $scope.message = "Något gick fel. Försök igen och kontakta support om felet kvarstår.";
          },
          // success or user-mail is already registered, or incorrect password
          function (resp) {
            $log.log(resp);
            var data = resp.data;
            $scope.success = data.success; // false or true
            $scope.message = data.message; // alert message
            if(data.success) { // close modal in 2 sec if successfull registration
              $timeout(function() {
                $state.go('login');
              }, 3000);
            } else { // not successfull (org or mail was registered or password incorrect)
              $scope.reset();
            }
          }
        );
      };

    }
  ]
);
