angular.module('useradministration.controllers.userctrl', [])

.controller('UserCtrl', ['$scope', '$state', '$modal', '$rootScope', '$timeout', 'userService', 'supplierFactory', 'orgService',
    function ($scope, $state, $modal, $rootScope, $timeout, userService, supplierFactory, orgService) {

      $scope.message = {};
      $scope.users = [];
      $scope.org = '';
      $scope.sortType     = 'name'; // set the default sort type
      $scope.sortReverse  = false;  // set the default sort order
      $rootScope.firstStateTitle = 'Användaradministration';
      $scope.alerts = [];

      //First get the organisation and store it in the scope and then get the users from that organisation.
      orgService.getCurrentOrg(
        function (err) { 
        /* TODO: handle error and push an alert */ 
        },
        function (resp) {
          if(resp.data.data.name) {
            var org = resp.data.data.name;
            userService.all(org,
              function (err) { 
              /* TODO: should handle error */ 
              },
              function (resp) {
                $scope.users = resp.data.data;
                console.log($scope.users);
              }
            );
          }
        }
      );

      //Closes the alert bow
      $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
      };

      //Delete user by user_id
      $scope.deleteUser = function (user_id) {
        userService.delete(user_id,
          function (err) { 
            //Push a new alert
            $scope.alerts.push({
              msg: 'Tusan! Kunde inte ta bort användaren. :(', 
              type: 'danger'
            });
          },
          function (resp) {
            var data = resp.data;

            //Push a new alert
            var alert = {};
            console.log(data);
            alert.msg = data.message;
            if (data.success) {
              alert.type = 'success';
            } else {
              alert.type = 'danger';
            };
            $scope.alerts.push({
              msg: alert.msg, 
              type: alert.type
            })

            //Remove the deleted user from the scope
            for(var i = $scope.users.length - 1; i >= 0; i--){
              if($scope.users[i]._id == user_id){
                  console.log($scope.users[i]._id);
                  $scope.users.splice(i,1);
              }
            } //End for-loop

          } 
        ); //End userService.delete()
      }; //End scope.deleteUser()

      $scope.openRemoveModal = function (user) {

        var modalInstance = $modal.open({
          templateUrl: 'modules/useradministration/partials/removeuser-modal.tpl.html',
          controller: 'RemoveUserModalCtrl',
          size: 'sm',
          resolve: {
            user: function () {
              return user;
            }
          }
        });

        modalInstance.result.then(function (clickedOk) {
          console.log(clickedOk);
          console.log(user._id);
          if(clickedOk) {
            $scope.deleteUser(user._id);
          };
        }, function () {
          //$log.info('Modal dismissed at: ' + new Date());
        });
      };

      //Function for opening the invitation modal
      $scope.openInviteModal = function () {
        var inviteModal = $modal.open({
          templateUrl: 'modules/useradministration/partials/invitation-modal.tpl.html',
          controller: 'InvitationModalInstanceCtrl'
        });
        inviteModal.result.then(function (data) {
          $state.go("anvandare");
        });
      };

    //End controller
    }
  ]
);

