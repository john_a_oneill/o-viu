angular.module('modules.useradministration', [
    'useradministration.controllers.invitationctrl',
    'useradministration.controllers.invitationmodalinstancectrl',
    'useradministration.controllers.removeusermodalctrl',
    'common',
    'useradministration.controllers.userctrl',
    'useradministration.controllers.userregistrationctrl',
    'useradministration.controllers.singlesuserctrl',
    ])

.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

  $stateProvider.state('inbjudan', {
    url: '/inbjudan',
    views: {
      'main@': {
        controller: 'InvitationCtrl',
        templateUrl: 'modules/useradministration/partials/invitation.tpl.html'
      }
    },
    access: {
      type: "private",
      groups: "admin"
    }
  });

    $stateProvider.state('anvandare', {
    url: '/anvandare',
    views: {
      'main': {
        controller: 'UserCtrl',
        templateUrl: 'modules/useradministration/partials/users.tpl.html'
      }
    },
    access: {
      type: "private",
      groups: "all"
    }
  });

  $stateProvider.state('invitationconfirmation', {
    url: '/invitationconfirmation/:orgid/:candid/:hash',
    views: {
      'main': {
        controller: 'UserRegistrationCtrl',
        templateUrl: 'modules/useradministration/partials/user-registration.tpl.html'
      }
    },
    access: {
      type: "public",
      groups: "all"
    }
  });

  $stateProvider.state('anvandare.enanvandare', {
    url: '/:user_id',
    views: {
      "main@": {
        controller: 'SingleUserCtrl',
        templateUrl: 'modules/useradministration/partials/single-user.tpl.html',
      }
    },
    access: {
      type: "private",
      groups: "all"
    }
  })

}]);
